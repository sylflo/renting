// @ts-check

import react from "eslint-plugin-react";
import react_hooks from 'eslint-plugin-react-hooks';
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import globals from "globals";
import tsParser from "@typescript-eslint/parser";

export default tseslint.config(
  eslint.configs.recommended,
  tseslint.configs.recommended,
  // tseslint.configs.stylisticTypeChecked,
  // tseslint.configs.strictTypeChecked,
  {
    plugins: {
      react,
      "react-hooks": react_hooks,
    },
    settings: {
      react: {
        version: "detect",
      }
    },
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.jest,
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parser: tsParser,
    ecmaVersion: 2018,
    sourceType: "module",
      parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        project: "./tsconfig.json",
      },
    },
    rules: {
      "@typescript-eslint/no-explicit-any": "error"
    }
  },
);
