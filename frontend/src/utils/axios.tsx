import { instance as axios } from "../config";

const setupAxiosInterceptors = () => {
  axios.interceptors.response.use(
    (response) => response,
    (error) => {
      const originalRequest = error.config;
      if (!error.response || error.response.status >= 500) {
        window.location.href = "/error";
      }
      if (error.response.status > 401) {
        window.location.href = "/404";
      }
      if (error.response.status === 401 && !originalRequest._retry) {
        if (originalRequest.url === "/login") {
          return Promise.reject(error);
        }
        localStorage.setItem("access_token", "");
        window.location.href = "/login";
        window.location.reload();
      }
      // return Error object with Promise
      return Promise.reject(error);
    },
  );

  axios.interceptors.request.use(
    (config) => {
      const accessToken = localStorage.getItem("access_token");
      if (accessToken !== "") {
        // eslint-disable-next-line no-param-reassign
        config.headers.Authorization = `Bearer ${accessToken}`;
      }
      return config;
    },
    (error) => {
      Promise.reject(error);
    },
  );
};

export default setupAxiosInterceptors;
