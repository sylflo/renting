import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
  Outlet,
} from "react-router-dom";
import Drawer from "../components/Drawer";
import Booking from "../pages/Booking";
import Home from "../pages/Home";
import Rental from "../pages/Rental";
import Login from "../pages/Login";
import NotFoundPage from "../pages/NotFoundPage";
import NetworkErrorPage from "../pages/ServerError";

const DrawerWrapper = () => {
  return (
    <Drawer>
      <Outlet /> {/* This will render the nested routes */}
    </Drawer>
  );
};

const AppRoutes = () => {
  const isAuthenticated = !!localStorage.getItem("access_token");

  return (
    <Router>
      <Routes>
        {isAuthenticated ? (
          <Route path="/" element={<DrawerWrapper />}>
            <Route index element={<Home />} />
            <Route path="rental/:uid/booking" element={<Booking />} />
            <Route path="rental/:uid" element={<Rental />} />
            <Route path="error" element={<NetworkErrorPage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Route>
        ) : (
          <>
            <Route path="/" element={<Login />} />
            <Route path="/error" element={<NetworkErrorPage />} />
            {/* Redirect all other routes to login if not authenticated */}
            <Route path="*" element={<Navigate replace to="/" />} />
          </>
        )}
      </Routes>
    </Router>
  );
};

export default AppRoutes;
