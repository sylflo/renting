import React, { ReactElement, ReactNode } from "react";
import { Container, Paper, Box } from "@mui/material";

// TODO build as shared type
interface PropsFormContainer {
  children: ReactNode;
}

const FormContainer = ({ children }: PropsFormContainer): ReactElement => {
  return (
    <Container>
      <Container maxWidth="sm">
        <Paper elevation={10}>
          <Box p={3}>{children}</Box>
        </Paper>
      </Container>
    </Container>
  );
};

export default FormContainer;
