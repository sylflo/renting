import React, { ReactElement, ReactNode, useEffect, useState } from "react";
import { useLocation, Link, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import HomeIcon from "@mui/icons-material/Home";
import SettingsIcon from "@mui/icons-material/Settings";
import ScheduleIcon from "@mui/icons-material/Schedule";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import useSharedStyles from "../shared/styles/index";

interface PropsDrawer {
  children: ReactNode;
}

const drawerWidth = 240;
const MAIN_ITEMS = [{ name: "home", link: "/", icon: <HomeIcon /> }];

const getMenuRental = (uid: string) => {
  return [
    { name: "rental", link: `/rental/${uid}`, icon: <SettingsIcon /> },
    {
      name: "booking",
      link: `/rental/${uid}/booking`,
      icon: <ScheduleIcon />,
    },
  ];
};

const getCurrentRoute = (pathname: string) => {
  let currentRoute = pathname.split("/").pop();
  if (pathname === "/") {
    currentRoute = "home";
  }
  if (pathname.split("/").length === 3) {
    currentRoute = "rental";
  }
  return currentRoute;
};

const PermanentDrawerLeft = ({ children }: PropsDrawer): ReactElement => {
  const { t } = useTranslation();
  const { classes } = useSharedStyles();
  const location = useLocation();
  const currentRoute = getCurrentRoute(location.pathname);
  const navigate = useNavigate();
  const [title, setTitle] = useState<string>("home");

  useEffect(() => {
    switch (true) {
      case location.pathname.includes("booking"):
        setTitle("booking");
        break;
      case location.pathname.includes("rental"):
        setTitle("rental");
        break;
      default:
        setTitle("home");
    }
  }, [location.pathname]);

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px` }}
      >
        <Toolbar>
          <Typography variant="h6" noWrap component="div">
            {t(title)}
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        variant="permanent"
        anchor="left"
      >
        <Toolbar />
        <Divider />
        <List>
          {MAIN_ITEMS.map((item) => (
            <Link className={classes.link} to={item.link} key={item.name}>
              <ListItemButton>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={t(item.name)} />
              </ListItemButton>
            </Link>
          ))}
        </List>
        <Divider />
        {location.pathname.includes("rental") &&
          getMenuRental(location.pathname.split("/")[2]).map((item) => (
            <Link
              className={`${classes.link} ${
                item.name === currentRoute ? classes.active : null
              }`}
              to={item.link}
              key={item.name}
            >
              <ListItemButton>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={t(item.name)} />
              </ListItemButton>
            </Link>
          ))}
        <Divider />
        <ListItemButton
          onClick={() => {
            window.localStorage.removeItem("access_token");
            window.localStorage.removeItem("refresh_token");
            navigate("/");
            window.location.reload();
          }}
        >
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          <ListItemText primary={t("logout")} />
        </ListItemButton>
      </Drawer>
      <Box
        component="main"
        sx={{ flexGrow: 1, bgcolor: "background.default", p: 3 }}
      >
        <Toolbar />
        <div>{children}</div>
      </Box>
    </Box>
  );
};

export default PermanentDrawerLeft;
