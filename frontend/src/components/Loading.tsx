import React, { ReactElement } from "react";
import { CircularProgress, Grid } from "@mui/material";

interface PropsLoading {
  fullHeight: boolean;
}

const Loading = ({ fullHeight }: PropsLoading): ReactElement => {
  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      style={fullHeight ? { minHeight: "100vh" } : { padding: "10px" }}
    >
      <Grid item xs={3}>
        <CircularProgress />
      </Grid>
    </Grid>
  );
};

export default Loading;
