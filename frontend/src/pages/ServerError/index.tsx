// NetworkErrorPage.js
import React, { ReactElement } from "react";
import Typography from "@mui/material/Typography";

const NetworkErrorPage = (): ReactElement => {
  return (
    <div>
      <Typography variant="h4" color="error">
        The server could not process your request
      </Typography>
      <Typography variant="body1">
        Oops! It seems there was a problem connecting to the server. Please try
        again later.
      </Typography>
    </div>
  );
};

export default NetworkErrorPage;
