import { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import useSWR, { mutate } from "swr";
import { instance as axios, fetcher } from "../../config";
import { ParamTypes } from "../../shared/types/misc";
import {
  Rental as RentalType,
  RentalFile as RentalFileType,
} from "../../shared/types/Rental";

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
const useRental = (t: any) => {
  const { uid } = useParams<ParamTypes>();
  const { data: rental } = useSWR(`/rentals/${uid}`, fetcher, {
    suspense: true,
  });

  const [files, setFiles] = useState<File[]>([]);
  const [fileToDelete, setFileToDelete] = useState<File | null>(null);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [timezones, setTimezones] = useState<string[]>([]);

  const apiCreateImage = async (data: RentalType) => {
    const res = await axios.put(`/rentals/${uid}`, {
      ...data,
      checkin_time: data.checkin_time.toFormat("HH:mm"),
      checkout_time: data.checkout_time.toFormat("HH:mm"),
    });
    if (files.length !== 0) {
      const formData = new FormData();
      files.forEach((file: File) => {
        formData.append("files", file);
      });
      await axios.post(`/rentals/${uid}/files`, formData);
    }
    mutate(`/rentals/${uid}`, rental);
    toast.success(t("rental_updated", { title: res.data.title }));
  };

  const apiDeleteImage = async () => {
    if (fileToDelete === null) {
      return;
    }
    const filename = fileToDelete.name;
    await axios.delete(`/rentals/${uid}/files/${filename}`);
    setFiles(files.filter((item: File) => item.name !== filename));
    setOpenConfirmDialog(false);
    toast.success(t("image_deleted", { filename }));
  };

  useEffect(() => {
    const apiTimezones = async () => {
      const res = await axios.get("/timezones");
      setTimezones(res.data);
    };

    const setBlobFromUrl = async () => {
      const remoteImages = await Promise.all(
        rental.files.map((filename: RentalFileType) => {
          return axios.get(
            `/static/rentals/${uid}/files/${filename.filename}`,
            {
              responseType: "blob",
            },
          );
        }),
      );
      setFiles(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        remoteImages.map((file: any, index: number) => {
          const blob: Blob = file.data;
          const newFile = new File([blob], rental.files[index].filename, {
            type: blob.type,
          });
          return Object.assign(newFile, {
            preview: URL.createObjectURL(blob),
          });
        }),
      );
    };
    setBlobFromUrl();
    apiTimezones();
  }, [rental, uid]);

  return {
    rental,
    files,
    setFiles,
    fileToDelete,
    setFileToDelete,
    openConfirmDialog,
    setOpenConfirmDialog,
    apiCreateImage,
    apiDeleteImage,
    timezones,
  };
};

export default useRental;
