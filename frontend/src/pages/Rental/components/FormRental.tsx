import React, { useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import { MobileTimePicker } from "@mui/x-date-pickers/MobileTimePicker";

import {
  Button,
  Box,
  Grid,
  TextField,
  Select,
  MenuItem,
  Autocomplete,
} from "@mui/material";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { phoneRegExp } from "../../../shared/yup";
import { DateTime } from "luxon";

const validationSchema = yup.object({
  title: yup.string().required("required"),
  landlord_email: yup.string().email().required("required"),
  landlord_phone_number: yup
    .string()
    .matches(phoneRegExp, "Phone number is not valid"),
  landlord_first_name: yup.string().required("required"),
  landlord_last_name: yup.string().required("required"),
  landlord_address: yup.string().required("required"),
  rental_address: yup.string().required("required"),
  cleaning_price: yup.number().min(0).required("required"),
  deposit_percentage: yup.number().min(0).max(100).required("required"),
  security_deposit: yup.number().min(0).required("required"),
  checkin_weekday: yup.number().min(0).max(6).required("required"),
  checkout_weekday: yup.number().min(0).max(6).required("required"),
  checkin_time: yup.mixed<DateTime<true>>().required("required"),
  checkout_time: yup.mixed<DateTime<true>>().required("required"),
  duration_before_sending_email: yup.number().min(0).required("required"),
  timezone: yup.string().required("required"),
});

interface PropsFormRental {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  t: any;
  rental: any;
  onSubmit: any;
  timezones: string[];
  children: any;
}

const time_to_luxon_date = (time: string) => {
  const [hours, minutes] = time.split(":");
  const luxon_date = DateTime.now().set({
    hour: parseInt(hours, 10),
    minute: parseInt(minutes, 10),
  });
  return luxon_date;
};

const FormRental = ({
  t,
  rental,
  onSubmit,
  timezones,
  children,
}: PropsFormRental) => {
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    setValue("title", rental.title);
    if (rental.landlord_email !== null) {
      // console.log()
      setValue("landlord_email", rental.landlord_email);
      setValue("landlord_phone_number", rental.landlord_phone_number);
      setValue("landlord_first_name", rental.landlord_first_name);
      setValue("landlord_last_name", rental.landlord_last_name);
      setValue("landlord_address", rental.landlord_address);
      setValue("rental_address", rental.rental_address);
      setValue("cleaning_price", rental.cleaning_price);
      setValue("deposit_percentage", rental.deposit_percentage);
      setValue("security_deposit", rental.security_deposit);
      setValue("cleaning_price", rental.cleaning_price);
      setValue("timezone", rental.timezone);
      setValue("checkin_weekday", rental.checkin_weekday);
      setValue("checkin_time", time_to_luxon_date(rental.checkin_time));
      setValue("checkout_weekday", rental.checkout_weekday);
      setValue("checkout_time", time_to_luxon_date(rental.checkout_time));
      setValue(
        "duration_before_sending_email",
        rental.duration_before_sending_email,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rental]);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Controller
              name="title"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("title")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.title}
                  helperText={errors.title ? t(errors.title.message) : ""}
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Controller
              name="landlord_email"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("landlord_email")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.landlord_email}
                  helperText={
                    errors.landlord_email
                      ? t(errors.landlord_email.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                name="landlord_phone_number"
                control={control}
                defaultValue=""
                render={({ field: { onChange, value } }) => (
                  <TextField
                    label={t("landlord_phone_number")}
                    value={value}
                    onChange={onChange}
                    error={!!errors.landlord_phone_number}
                    helperText={
                      errors.landlord_phone_number
                        ? t(errors.landlord_phone_number.message)
                        : ""
                    }
                  />
                )}
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Controller
              name="landlord_first_name"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("landlord_first_name")}
                  value={value}
                  onChange={onChange}
                  error={!!errors.landlord_first_name}
                  helperText={
                    errors.landlord_first_name
                      ? t(errors.landlord_first_name.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                name="landlord_last_name"
                control={control}
                defaultValue=""
                render={({ field: { onChange, value } }) => (
                  <TextField
                    label={t("landlord_last_name")}
                    value={value}
                    onChange={onChange}
                    error={!!errors.landlord_last_name}
                    helperText={
                      errors.landlord_last_name
                        ? t(errors.landlord_last_name.message)
                        : ""
                    }
                  />
                )}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="landlord_address"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("landlord_address")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.landlord_address}
                  helperText={
                    errors.landlord_address
                      ? t(errors.landlord_address.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="rental_address"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("rental_address")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.rental_address}
                  helperText={
                    errors.rental_address
                      ? t(errors.rental_address.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="cleaning_price"
              control={control}
              defaultValue={0}
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("cleaning_price")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.cleaning_price}
                  helperText={
                    errors.cleaning_price
                      ? t(errors.cleaning_price.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Controller
              name="deposit_percentage"
              control={control}
              defaultValue={0}
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("deposit_percentage")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.deposit_percentage}
                  helperText={
                    errors.deposit_percentage
                      ? t(errors.deposit_percentage.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                name="security_deposit"
                control={control}
                defaultValue={0}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    label={t("security_deposit")}
                    value={value}
                    onChange={onChange}
                    error={!!errors.security_deposit}
                    helperText={
                      errors.security_deposit
                        ? t(errors.security_deposit.message)
                        : ""
                    }
                  />
                )}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="timezone"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  options={timezones}
                  getOptionLabel={(option) => option}
                  onChange={(_, data) => {
                    field.onChange(data);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Choose an option"
                      variant="outlined"
                    />
                  )}
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                render={({ field }) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <Select {...field}>
                    <MenuItem value={0}>{t("monday")}</MenuItem>
                    <MenuItem value={1}>{t("tuesday")}</MenuItem>
                    <MenuItem value={2}>{t("wednesday")}</MenuItem>
                    <MenuItem value={3}>{t("thursday")}</MenuItem>
                    <MenuItem value={4}>{t("friday")}</MenuItem>
                    <MenuItem value={5}>{t("saturday")}</MenuItem>
                    <MenuItem value={6}>{t("sunday")}</MenuItem>
                  </Select>
                )}
                control={control}
                name="checkin_weekday"
                defaultValue={6}
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                name="checkin_time"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <MobileTimePicker
                    label={t("checkin")}
                    value={value || null}
                    onChange={onChange}
                  />
                )}
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                render={({ field }) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <Select {...field}>
                    <MenuItem value={0}>{t("monday")}</MenuItem>
                    <MenuItem value={1}>{t("tuesday")}</MenuItem>
                    <MenuItem value={2}>{t("wednesday")}</MenuItem>
                    <MenuItem value={3}>{t("thursday")}</MenuItem>
                    <MenuItem value={4}>{t("friday")}</MenuItem>
                    <MenuItem value={5}>{t("saturday")}</MenuItem>
                    <MenuItem value={6}>{t("sunday")}</MenuItem>
                  </Select>
                )}
                control={control}
                name="checkout_weekday"
                defaultValue={6}
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-end">
              <Controller
                name="checkout_time"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <MobileTimePicker
                    label={t("checkout")}
                    value={value || null}
                    onChange={onChange}
                  />
                )}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="duration_before_sending_email"
              control={control}
              defaultValue={0}
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("duration_before_sending_email")}
                  value={value}
                  onChange={onChange}
                  fullWidth
                  error={!!errors.duration_before_sending_email}
                  helperText={
                    errors.duration_before_sending_email
                      ? t(errors.duration_before_sending_email.message)
                      : ""
                  }
                />
              )}
            />
          </Grid>
          {children}
          <Grid item xs={12}>
            <Button type="submit" variant="contained" fullWidth>
              {t("submit")}
            </Button>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default FormRental;
