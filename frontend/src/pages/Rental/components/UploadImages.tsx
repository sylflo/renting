/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useMemo, Dispatch, SetStateAction, ReactElement } from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import { useDropzone } from "react-dropzone";
import { IconButton, ImageListItemBar } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const baseStyle = {
  flex: 1,
  display: "flex",
  // eslint-disable-next-line @typescript-eslint/prefer-as-const
  flexDirection: "column" as "column",
  alignItems: "center",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

interface PropsUploadImages {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
  setFiles: any;
  files: File[];
  setFileToDelete: Dispatch<SetStateAction<File | null>>;
  setOpenConfirmDialog: Dispatch<SetStateAction<boolean>>;
}

const UploadImages = ({
  t,
  setFiles,
  files,
  setFileToDelete,
  setOpenConfirmDialog,
}: PropsUploadImages): ReactElement => {
  const onDrop = (acceptedFiles: File[]) => {
    const allFiles = [...files, ...acceptedFiles];
    setFiles(
      allFiles.map((file: any) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        }),
      ),
    );
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({ accept: { "image/*": [] }, onDrop });
  const style: any = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept],
  );

  return (
    <div className="container">
      <div {...getRootProps({ style })}>
        <input {...getInputProps()} />
        <p>{t("files_booking_email")}</p>
      </div>
      <ImageList cols={3} rowHeight={164}>
        {files.map((file: any) => (
          <ImageListItem key={file.name}>
            <img src={file.preview} alt={file.filename} />
            <ImageListItemBar
              position="top"
              actionIcon={
                <IconButton
                  sx={{ color: "white" }}
                  aria-label="delete"
                  onClick={() => {
                    setFileToDelete(file);
                    setOpenConfirmDialog(true);
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              }
              actionPosition="left"
            />
          </ImageListItem>
        ))}
      </ImageList>
    </div>
  );
};

export default UploadImages;
