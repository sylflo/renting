import React, { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import { Grid } from "@mui/material";
import UploadImages from "./components/UploadImages";
import DialogConfirmation from "./components/DialogConfirmation";
import FormRental from "./components/FormRental";
import FormContainer from "../../components/FormContainer";
import useRental from "./hooks";

const Rental = (): ReactElement => {
  const { t } = useTranslation();

  const {
    rental,
    files,
    setFiles,
    fileToDelete,
    setFileToDelete,
    openConfirmDialog,
    setOpenConfirmDialog,
    apiCreateImage,
    apiDeleteImage,
    timezones,
  } = useRental(t);

  return (
    <>
      <FormContainer>
        <FormRental
          t={t}
          rental={rental}
          onSubmit={apiCreateImage}
          timezones={timezones}
        >
          <Grid item xs={12}>
            <UploadImages
              t={t}
              setFiles={setFiles}
              files={files}
              setFileToDelete={setFileToDelete}
              setOpenConfirmDialog={setOpenConfirmDialog}
            />
          </Grid>
        </FormRental>
        {fileToDelete && (
          <DialogConfirmation
            t={t}
            open={openConfirmDialog}
            setOpen={setOpenConfirmDialog}
            itemName={fileToDelete.name}
            apiDeleteImage={apiDeleteImage}
          />
        )}
      </FormContainer>
    </>
  );
};

export default Rental;
