import React, { ReactElement } from "react";
import { Typography } from "@mui/material";

const NotFoundPage = (): ReactElement => {
  return (
    <div>
      <Typography variant="h3">404 - Page Not Found</Typography>
      <Typography variant="body1">
        Sorry, the page you are looking for does not exist.
      </Typography>
    </div>
  );
};

export default NotFoundPage;
