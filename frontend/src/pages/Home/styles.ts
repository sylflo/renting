import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()(() => {
  return {
    root: {
      flexGrow: 1,
    },
    fab: {
      position: "fixed",
      bottom: 10,
      right: 10,
    },
    media: {
      height: 240,
    },
    link: {
      textDecoration: "none",
      color: "black",
    },
  };
});

export default useStyles;
