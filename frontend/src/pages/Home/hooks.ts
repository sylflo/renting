import { useState } from "react";
import { toast } from "react-toastify";
import useSWR, { mutate } from "swr";
import { useNavigate } from "react-router-dom";
import { InputsFormCreateRental } from "./types";
import { Rental as RentalType } from "../../shared/types/Rental";
import { instance as axios, fetcher } from "../../config";

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
const useHome = (t: any) => {
  const { data: rentals, error } = useSWR("/rentals", fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    errorRetryCount: 1,
  });
  const navigate = useNavigate();
  const [rental, setRental] = useState<RentalType | null>(null);
  const [openCreateDialog, setOpenCreateDialog] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);

  const apiCreateRental = async (data: InputsFormCreateRental) => {
    const res = await axios.post("/rentals", {
      title: data.title,
    });
    toast.success(t("rental_created", { title: data.title }));
    navigate(`/rental/${res.data.uid}`);
  };

  const apiDeleteRental = async (uid: string, title: string) => {
    await axios.delete(`/rentals/${uid}/`);
    setOpenDeleteDialog(false);
    toast.success(t("rental_deleted", { title }));
    setRental(null);
    mutate("/rentals", rentals);
  };

  return {
    error,
    rentals,
    rental,
    setRental,
    openCreateDialog,
    setOpenCreateDialog,
    openDeleteDialog,
    setOpenDeleteDialog,
    apiCreateRental,
    apiDeleteRental,
  };
};

export default useHome;
