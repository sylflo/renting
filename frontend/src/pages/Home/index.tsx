import React, { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import { Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RentalList from "./components/RentalList";
import DialogFormRental from "./components/DialogFormRental";
import DialogDeleteRental from "./components/DialogDeleteRental";
import useHome from "./hooks";
import useStyles from "./styles";

const Home = (): ReactElement => {
  const { t } = useTranslation();
  const { classes } = useStyles();
  const {
    error,
    rentals,
    rental,
    setRental,
    openCreateDialog,
    setOpenCreateDialog,
    openDeleteDialog,
    setOpenDeleteDialog,
    apiCreateRental,
    apiDeleteRental,
  } = useHome(t);

  if (error) return <div> {t("an error has occurred")}</div>;

  return (
    <>
      <Fab
        style={{ position: "fixed", bottom: 10, right: 10 }}
        color="primary"
        aria-label="add"
        onClick={() => {
          setOpenCreateDialog(true);
        }}
      >
        <AddIcon />
      </Fab>
      <RentalList
        t={t}
        classes={classes}
        setOpenDeleteDialog={setOpenDeleteDialog}
        setRental={setRental}
        rentals={rentals}
      />
      <DialogFormRental
        openDialog={openCreateDialog}
        setOpenDialog={setOpenCreateDialog}
        onSubmit={apiCreateRental}
        t={t}
      />
      <DialogDeleteRental
        openDialog={openDeleteDialog}
        setOpenDialog={setOpenDeleteDialog}
        confirm={apiDeleteRental}
        rental={rental}
        t={t}
      />
    </>
  );
};

export default Home;
