import React, { Dispatch, ReactElement, SetStateAction } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
} from "@mui/material";
import { Rental as RentalType } from "../../../shared/types/Rental";

export interface PropsDialogDeleteRental {
  openDialog: boolean;
  setOpenDialog: Dispatch<SetStateAction<boolean>>;
  confirm: (uid: string, title: string) => Promise<void>;
  rental: RentalType | null;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
}

const DialogDeleteRental = ({
  openDialog,
  setOpenDialog,
  confirm,
  rental,
  t,
}: PropsDialogDeleteRental): ReactElement | null => {
  if (!rental) {
    return null;
  }
  return (
    <Dialog
      onClose={() => {
        setOpenDialog(false);
      }}
      open={openDialog}
    >
      <DialogTitle>{t("delete_rental")}</DialogTitle>
      {rental !== null && (
        <DialogContent>
          {t("are_you_sure_you_want_to_delete_this_rental")}
        </DialogContent>
      )}
      <DialogActions>
        <Button
          onClick={() => {
            setOpenDialog(false);
          }}
          color="primary"
        >
          {t("cancel")}
        </Button>
        <Button
          color="primary"
          onClick={() => confirm(rental.uid, rental.title)}
        >
          {t("confirm")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogDeleteRental;
