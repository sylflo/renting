import React, { Dispatch, ReactElement, SetStateAction } from "react";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from "@mui/material";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { InputsFormCreateRental } from "../types";

interface PropsDialogFormRental {
  openDialog: boolean;
  setOpenDialog: Dispatch<SetStateAction<boolean>>;
  onSubmit: SubmitHandler<InputsFormCreateRental>;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
}

const DialogFormRental = ({
  openDialog,
  setOpenDialog,
  onSubmit,
  t,
}: PropsDialogFormRental): ReactElement => {
  const { control, handleSubmit } = useForm<InputsFormCreateRental>();

  return (
    <Dialog
      onClose={() => {
        setOpenDialog(false);
      }}
      open={openDialog}
    >
      <DialogTitle>{t("create_a_new_rental")}</DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <Controller
            name="title"
            defaultValue=""
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField label={t("title")} value={value} onChange={onChange} />
            )}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setOpenDialog(false);
            }}
            color="primary"
          >
            {t("cancel")}
          </Button>
          <Button color="primary" type="submit">
            {t("confirm")}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default DialogFormRental;
