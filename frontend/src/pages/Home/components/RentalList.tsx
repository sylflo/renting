import React, { Dispatch, ReactElement, SetStateAction } from "react";
import { Link } from "react-router-dom";
import {
  Grid,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CardActions,
  Typography,
  Button,
  IconButton,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { Rental as RentalType } from "../../../shared/types/Rental";

interface PropsRentalList {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  t: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  classes: any;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
  setRental: Dispatch<SetStateAction<RentalType | null>>;
  rentals: [RentalType];
}

const RentalList = ({
  t,
  classes,
  setOpenDeleteDialog,
  setRental,
  rentals,
}: PropsRentalList): ReactElement => {
  return (
    <Grid container spacing={2} className={classes.root}>
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={4}>
          {rentals.map((item: RentalType) => {
            return (
              <Grid xs={2} item key={item.uid}>
                <Card>
                  <CardActionArea>
                    <Link className={classes.link} to={`/rental/${item.uid}`}>
                      <CardMedia
                        image="https://picsum.photos/200"
                        title="Rental preview image"
                        className={classes.media}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h6" component="h2">
                          {item.title}
                        </Typography>
                      </CardContent>
                    </Link>
                  </CardActionArea>
                  <CardActions disableSpacing>
                    <IconButton
                      onClick={() => {
                        setOpenDeleteDialog(true);
                        setRental(item);
                      }}
                      aria-label="Delete rental"
                    >
                      <DeleteIcon />
                    </IconButton>
                    <Link className={classes.link} to={`/rental/${item.uid}`}>
                      <Button
                        style={{ marginLeft: "auto" }}
                        variant="contained"
                        color="primary"
                      >
                        {t("access_rental")}
                      </Button>
                    </Link>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default RentalList;
