import React, { ReactElement, useEffect, useState } from "react";
import { useParams } from "react-router";
import { toast } from "react-toastify";
import useSWR, { mutate } from "swr";
import { useTranslation } from "react-i18next";
import { Fab, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { instance as axios, fetcher } from "../../config";
import { Booking as BookingType } from "../../shared/types/Booking";
import { BookingForm as BookingFormType } from "./types";
import { ParamTypes } from "../../shared/types/misc";
import BookingList from "./components/BookingList";
import Booking from "./components/Booking";
import DialogFormBooking from "./components/DialogFormBooking";
import DialogFormBookingDelete from "./components/DialogFormBookingDelete";

const BookingPage = (): ReactElement => {
  const { t } = useTranslation();
  const [pageIndex, setPageIndex] = useState(0);
  const { uid } = useParams<ParamTypes>();
  const { data: bookings } = useSWR(
    `/rental/${uid}/bookings?skip=${pageIndex}&limit=10`,
    fetcher,
    {
      suspense: true,
      shouldRetryOnError: false,
      errorRetryCount: 1,
    },
  );
  const [loading, setLoading] = useState(false);
  const [openCreateDialog, setOpenCreateDialog] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [selectedBooking, setSelectedBooking] = useState<BookingType | null>(
    null,
  );

  const deleteBooking = async () => {
    await axios.delete(
      `/rentals/${uid}/bookings/${selectedBooking?.start_at}/${selectedBooking?.end_at}`,
    );
    toast.success(
      t("booking_deleted", {
        start_at: selectedBooking?.start_at,
        end_at: selectedBooking?.end_at,
      }),
    );
    mutate(`/rental/${uid}/bookings?skip=${pageIndex}&limit=10`, bookings);
    setOpenDeleteDialog(false);
  };

  const createBooking = async (
    data: BookingFormType,
    startDate: any,
    endDate: any,
  ) => {
    const { test_email: testEmail, ...newData } = data;
    let endpoint = `/rentals/${uid}/bookings`;
    let toastMessage = t("booking_added");

    // TODO check if we can do this more properly using react hook form
    if (data.total_adult === 0 && data.total_minor === 0) {
      toast.error(t("at_least_one_customer"));
      return;
    }
    if (testEmail) {
      endpoint = `/rentals/${uid}/bookings/emails/confirmation/test`;
      toastMessage = t("booking_email_test");
    }

    setLoading(true);
    await axios.post(endpoint, {
      ...newData,
      start_at: startDate.toFormat("yyyy-MM-dd"),
      end_at: endDate.toFormat("yyyy-MM-dd"),
      when_to_send_email: data.when_to_send_email.toFormat("yyyy-MM-dd"),
    });
    setLoading(false);
    toast.success(toastMessage);
    setOpenCreateDialog(false);
    mutate(`/rental/${uid}/bookings?skip=${pageIndex}&limit=10`, bookings);
  };

  useEffect(() => {
    if (bookings.bookings.length > 0) {
      setSelectedBooking(bookings.bookings[0]);
    } else {
      setSelectedBooking(null);
    }
  }, [bookings, pageIndex]);

  return (
    <>
      <Fab
        style={{ position: "fixed", bottom: 10, right: 10 }}
        color="primary"
        aria-label="add"
        onClick={() => {
          setOpenCreateDialog(true);
        }}
      >
        <AddIcon />
      </Fab>
      <Grid container spacing={3}>
        {selectedBooking !== null && (
          <>
            <Grid item xs={4}>
              <BookingList
                setSelectedBooking={setSelectedBooking}
                selectedBooking={selectedBooking}
                bookings={bookings.bookings}
                pages={bookings.pages}
                setPageIndex={setPageIndex}
                setOpenDeleteDialog={setOpenDeleteDialog}
                t={t}
              />
            </Grid>
            <Grid item xs={8}>
              <Booking booking={selectedBooking} t={t} />
            </Grid>
          </>
        )}
      </Grid>
      <DialogFormBooking
        openDialog={openCreateDialog}
        setOpenDialog={setOpenCreateDialog}
        createBooking={createBooking}
        loading={loading}
        t={t}
      />
      <DialogFormBookingDelete
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        itemName={t("selected_booking", {
          start_at: selectedBooking?.start_at,
          end_at: selectedBooking?.end_at,
        })}
        apiDeleteItem={deleteBooking}
        item={selectedBooking}
        t={t}
      />
    </>
  );
};

export default BookingPage;
