/* eslint-disable */
import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

type PropsDialogConfirmation = {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
  open: boolean;
  setOpen: any;
  itemName: any;
  apiDeleteItem: any;
  item: any;
};

const DialogConfirmation = ({
  t,
  open,
  setOpen,
  itemName,
  apiDeleteItem,
}: PropsDialogConfirmation) => {
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>{t("confirm")}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t("sure_delete")} {itemName}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("cancel")}
          </Button>
          <Button onClick={() => apiDeleteItem()} color="primary" autoFocus>
            {t("confirm")}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogConfirmation;
