import React, { ReactElement } from "react";
import { Button, Divider, Grid, Link, Typography } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import GetAppIcon from "@mui/icons-material/GetApp";
import NightsStayIcon from "@mui/icons-material/NightsStay";
import { instance as axios } from "../../../config";
import {
  Booking as BookingType,
  BookingFile as BookingFileType,
} from "../../../shared/types/Booking";

interface PropsBooking {
  booking: BookingType;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
}

const Booking = ({ booking, t }: PropsBooking): ReactElement => {
  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography component="h1" variant="h4">
              {booking.customer.last_name} {booking.customer.first_name}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              component="p"
              variant="body1"
              display="flex"
              alignItems="center"
              flexWrap="wrap"
            >
              {booking.start_at} <ArrowForwardIcon /> {booking.end_at} (
              {booking.nights} <NightsStayIcon />)
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <div>
              {booking.total_person} {t("persons")} : {booking.total_adult}{" "}
              {t("adults")}, {booking.total_minor} {t("minors")}
            </div>
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12}>
            <div>
              {t("will_email_be_sent")}:{" "}
              {booking.should_send_email ? t("yes") : t("no")} <br />
              {t("when_will_email_be_sent")}: {booking.when_to_send_email}{" "}
              <br />
              {t("has_email_been_sent")}:{" "}
              {booking.confirmation_email_sent ? t("yes") : t("no")} <br />
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Divider />
      </Grid>
      <Grid item xs={6}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="h2" variant="h5">
              {t("client")}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6}>
            {t("fullname")}
          </Grid>
          <Grid item xs={6}>
            {booking.customer.last_name} {booking.customer.first_name}
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6}>
            {t("phone_number")}
          </Grid>
          <Grid item xs={6}>
            {booking.customer.phone_number}
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6}>
            {t("email")}
          </Grid>
          <Grid item xs={6}>
            {booking.customer.email}
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6}>
            {t("address")}
          </Grid>
          <Grid item xs={6}>
            {booking.customer.address}
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6}>
            {t("language")}
          </Grid>
          <Grid item xs={6}>
            {booking.customer.language}
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={6}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="h2" variant="h5">
              {t("files")}
            </Typography>
            {booking.files.map((file: BookingFileType) => (
              <div key={file.position}>
                <Button variant="outlined" startIcon={<GetAppIcon />}>
                  <Link
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`${axios.defaults.baseURL}/${file.get_path}`}
                  >
                    {t("download")} {file.filename}
                  </Link>
                </Button>
              </div>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Booking;
