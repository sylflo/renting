import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { instance as axios } from "../../../../config";
import { DateTime } from "luxon";
import { Customer as CustomerType } from "../../../../shared/types/Customer";
import { BookingForm as BookingFormType } from "../../types";
import { phoneRegExp } from "../../../../shared/yup";
import { useParams } from "react-router";
import { ParamTypes } from "../../../../shared/types/misc";

const validationSchema = yup.object({
  email: yup.string().email().required("required"),
  phone_number: yup
    .string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("required"),
  first_name: yup.string().required("required"),
  last_name: yup.string().required("required"),
  address: yup.string().required("required"),
  language: yup.string().oneOf(["EN", "FR"]).required("required"),
  booking_price: yup.number().min(0).required("required"),
  total_adult: yup.number().min(0).required("required"),
  total_minor: yup.number().min(0).required("required"),
  when_to_send_email: yup
    .mixed<DateTime<true> | DateTime<false>>()
    .required("required"),
  // TODO total_adult + total_minor != -1
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
const useBooking = (createBooking: any) => {
  const { uid } = useParams<ParamTypes>();
  const [customer, setCustomer] = useState<CustomerType | null>(null);
  const [valueDate, setValueDate] = useState<[any | null, any | null]>([
    null,
    null,
  ]);
  const [rentalTimezone, setRentalTimezone] = useState<string>("");
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    if (customer !== null) {
      setValue("email", customer.email);
      setValue("first_name", customer.first_name);
      setValue("phone_number", customer.phone_number);
      setValue("last_name", customer.last_name);
      setValue("address", customer.address);
      setValue("language", customer.language);
    } else {
      setValue("first_name", "");
      setValue("phone_number", "");
      setValue("last_name", "");
      setValue("address", "");
      setValue("language", "FR");
    }
  }, [customer, setValue]);

  useEffect(() => {
    const getRental = async () => {
      try {
        const res = await axios.get(`rentals/${uid}`);
        let newDate = null;
        if (valueDate[0] === null) {
          newDate = DateTime.now();
        } else {
          newDate = DateTime.fromJSDate(valueDate[0].toDate());
          newDate = newDate.minus({
            days: res.data.duration_before_sending_email,
          });
        }
        setRentalTimezone(res.data.timezone);
        setValue("when_to_send_email", newDate);
      } catch (e) {
        console.error(e);
      }
    };
    getRental();
  }, [uid, valueDate, setValue]);

  const onSubmit = async (data: BookingFormType) => {
    const startDate = DateTime.fromJSDate(data.date_range.from);
    const endDate = DateTime.fromJSDate(data.date_range.to);

    await createBooking(data, startDate, endDate);
  };

  return {
    handleSubmit,
    onSubmit,
    control,
    errors,
    setValueDate,
    valueDate,
    customer,
    setCustomer,
    rentalTimezone,
  };
};

export default useBooking;
