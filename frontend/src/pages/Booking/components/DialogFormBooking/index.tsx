/* eslint-disable */
import React, { Dispatch, ReactElement, SetStateAction } from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Box,
  Button,
  Select,
  MenuItem,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import { Controller } from "react-hook-form";
import useBooking from "./hooks";
import Loading from "../../../../components/Loading";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DayPicker } from "react-day-picker";
import "react-day-picker/style.css";

type PropsDialogFormBooking = {
  openDialog: boolean;
  setOpenDialog: Dispatch<SetStateAction<boolean>>;
  createBooking: any;
  // onSubmit: SubmitHandler<InputsFormCreateRental>;
  loading: boolean;
  t: any;
};

const Booking = ({
  setOpenDialog,
  openDialog,
  createBooking,
  loading,
  t,
}: PropsDialogFormBooking): ReactElement => {
  const {
    handleSubmit,
    onSubmit,
    control,
    errors,
    rentalTimezone,
  }: any = useBooking(createBooking);
  return (
    <Dialog
      onClose={() => setOpenDialog(false)}
      open={openDialog}
      style={{ margin: "100px" }}
    >
      <DialogTitle>{t("create_a_new_booking")}</DialogTitle>
      {loading ? (
        <Loading fullHeight={false} />
      ) : (
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogContent>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Controller
                  name="email"
                  control={control}
                  defaultValue=""
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      label={t("email")}
                      value={value}
                      onChange={onChange}
                      error={!!errors.email}
                      helperText={errors.email ? t(errors.email.message) : ""}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Box display="flex" justifyContent="flex-end">
                  <Controller
                    name="phone_number"
                    control={control}
                    defaultValue=""
                    render={({ field: { onChange, value } }) => (
                      <TextField
                        label={t("phone_number")}
                        value={value}
                        onChange={onChange}
                        error={!!errors.phone_number}
                        helperText={
                          errors.phone_number
                            ? t(errors.phone_number.message)
                            : ""
                        }
                      />
                    )}
                  />
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="first_name"
                  control={control}
                  defaultValue=""
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      label={t("first_name")}
                      value={value}
                      onChange={onChange}
                      error={!!errors.first_name}
                      helperText={
                        errors.first_name ? t(errors.first_name.message) : ""
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Box display="flex" justifyContent="flex-end">
                  <Controller
                    name="last_name"
                    control={control}
                    defaultValue=""
                    render={({ field: { onChange, value } }) => (
                      <TextField
                        label={t("last_name")}
                        value={value}
                        onChange={onChange}
                        error={!!errors.last_name}
                        helperText={
                          errors.last_name ? t(errors.last_name.message) : ""
                        }
                      />
                    )}
                  />
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="address"
                  control={control}
                  defaultValue=""
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      label={"address"}
                      value={value}
                      onChange={onChange}
                      error={!!errors.address}
                      helperText={
                        errors.address ? t(errors.address.message) : ""
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Box display="flex" justifyContent="flex-end">
                  <Controller
                    render={({ field }) => (
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      <Select {...field}>
                        <MenuItem value="FR">{t("french")}</MenuItem>
                        <MenuItem value="EN">{t("english")}</MenuItem>
                      </Select>
                    )}
                    control={control}
                    name="language"
                    defaultValue="FR"
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="date_range"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <DayPicker
                      mode="range"
                      selected={value}
                      onSelect={(selectedRange) =>
                        onChange(selectedRange || undefined)
                      } // Handle null as undefined
                      required
                      timeZone={rentalTimezone}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Box display="flex" justifyContent="flex-start">
                  <Controller
                    name="total_adult"
                    control={control}
                    defaultValue={0}
                    render={({ field: { onChange, value } }) => (
                      <TextField
                        label={t("total_adult")}
                        value={value}
                        onChange={onChange}
                        error={!!errors.total_adult}
                        helperText={
                          errors.total_person
                            ? t(errors!.total_adult!.message)
                            : ""
                        }
                      />
                    )}
                  />
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Box display="flex" justifyContent="flex-end">
                  <Controller
                    name="total_minor"
                    control={control}
                    defaultValue=""
                    render={({ field: { onChange, value } }) => (
                      <TextField
                        label={t("total_minor")}
                        value={value}
                        onChange={onChange}
                        error={!!errors.total_minor}
                        helperText={
                          errors.total_person
                            ? t(errors!.total_minor!.message)
                            : ""
                        }
                      />
                    )}
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="booking_price"
                  control={control}
                  defaultValue=""
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      fullWidth
                      label={t("booking_price")}
                      value={value}
                      onChange={onChange}
                      error={!!errors.booking_price}
                      helperText={
                        errors.booking_price
                          ? t(errors.booking_price.message)
                          : ""
                      }
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="when_to_send_email"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      label={t("when_to_send_email")}
                      value={value}
                      onChange={onChange}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="should_send_email"
                  control={control}
                  defaultValue={true}
                  render={({ field: { onChange, value } }) => (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={value}
                          onChange={onChange}
                          name="should_send_email"
                        />
                      }
                      label={t("should_send_email")}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="test_email"
                  control={control}
                  defaultValue={false}
                  render={({ field: { onChange, value } }) => (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={value}
                          onChange={onChange}
                          name="test_email"
                        />
                      }
                      label={t("test_email")}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="tourist_tax"
                  control={control}
                  defaultValue={true}
                  render={({ field: { onChange, value } }) => (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={value}
                          onChange={onChange}
                          name="tourist_tax"
                        />
                      }
                      label={t("tourist_tax")}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Controller
                  name="cleaning"
                  control={control}
                  defaultValue={true}
                  render={({ field: { onChange, value } }) => (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={value}
                          onChange={onChange}
                          name="cleaning"
                        />
                      }
                      label={t("cleaning")}
                    />
                  )}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="contained"
                  style={{ width: "100%" }}
                >
                  {t("submit")}
                </Button>
              </Grid>
            </Grid>
          </DialogActions>
        </form>
      )}
    </Dialog>
  );
};

export default Booking;
