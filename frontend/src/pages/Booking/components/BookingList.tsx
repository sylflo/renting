import React, { Dispatch, ReactElement, SetStateAction } from "react";
import {
  Grid,
  List,
  ListItemButton,
  ListItemText,
  Pagination,
  Typography,
  IconButton,
} from "@mui/material";
import NightsStayIcon from "@mui/icons-material/NightsStay";
import PersonIcon from "@mui/icons-material/Person";
import DeleteIcon from "@mui/icons-material/Delete";
import { Booking as BookingType } from "../../../shared/types/Booking";

interface PropsBookingList {
  bookings: [BookingType];
  selectedBooking: BookingType;
  setSelectedBooking: Dispatch<SetStateAction<BookingType | null>>;
  pages: number;
  setPageIndex: Dispatch<SetStateAction<number>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  t: any;
}

const BookingList = ({
  bookings,
  selectedBooking,
  setSelectedBooking,
  pages,
  setPageIndex,
  setOpenDeleteDialog,
  t,
}: PropsBookingList): ReactElement => {
  return (
    <>
      <List>
        {bookings.map((booking: BookingType) => {
          return (
            <ListItemButton
              key={booking.start_at}
              onClick={() => {
                setSelectedBooking(booking);
              }}
              selected={selectedBooking.start_at === booking.start_at}
            >
              <Grid>
                <ListItemText>
                  <Typography variant="h5" component="h2">
                    {booking.customer.last_name} {booking.customer.first_name}
                  </Typography>
                </ListItemText>
                <ListItemText>
                  <Typography component="span">
                    {t("booking_date_range", {
                      start_at: booking.start_at,
                      end_at: booking.end_at,
                    })}
                  </Typography>
                </ListItemText>
                <ListItemText>
                  <Typography component="span">
                    {booking.nights} <NightsStayIcon /> {booking.total_person}{" "}
                    <PersonIcon />
                    <IconButton
                      onClick={() => {
                        setOpenDeleteDialog(true);
                      }}
                      aria-label="Delete rental"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Typography>
                </ListItemText>
              </Grid>
            </ListItemButton>
          );
        })}
      </List>
      <Pagination
        count={pages}
        onChange={(_, value) => {
          setPageIndex(value - 1);
        }}
      />
    </>
  );
};

export default BookingList;
