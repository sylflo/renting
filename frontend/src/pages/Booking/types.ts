import { Customer as CustomerType } from "../../shared/types/Customer";
import { DateTime } from "luxon";

interface Booking {
  test_email: boolean;
  booking_price: number;
  total_adult: number;
  total_minor: number;
  date_range: any;
  when_to_send_email: DateTime;
}
export type BookingForm = CustomerType & Booking;
