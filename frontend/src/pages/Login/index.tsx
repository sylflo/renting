import React, { ReactElement } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { makeStyles } from "tss-react/mui";
import { Avatar, Grid, TextField, Button } from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { instance as axios } from "../../config";
import { Login as LoginType } from "../../shared/types/Login";

import background from "./background.jpeg";

const useStyles = makeStyles()(() => {
  return {
    root: {
      height: "100vh",
    },
    image: {
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundPosition: "center",
    },
    paper: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  };
});

const validationSchema = yup.object({
  email: yup.string().email().required("required"),
  password: yup.string().required("required"),
});

const Login = (): ReactElement => {
  const { classes } = useStyles();
  const { t } = useTranslation();
  const {
    control,
    handleSubmit,
    formState: { errors },
  }: any = useForm({
    resolver: yupResolver(validationSchema),
  });

  const apiLogin = async (data: LoginType) => {
    try {
      const res = await axios.post("/login", data);
      toast.success(t("Logged in"));
      localStorage.setItem("access_token", res.data.tokens.access_token);
      localStorage.setItem("refresh_token", res.data.tokens.refresh_token);
      // Redirect to Google OAuth
      window.location.href = res.data.auth_url;
      // eslint-disable-next-line
    } catch (e: any) {
      toast.error(t(e.response.data.detail));
      // eslint-disable-next-line no-console
      console.error(e);
    }
  };

  return (
    <Grid container component="main" className={classes.root}>
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        lg={8}
        className={classes.image}
        style={{ backgroundImage: `url(${background})` }}
      />
      <Grid item xs={12} sm={8} md={5} lg={4}>
        <div className={classes.paper}>
          <Avatar style={{ marginTop: "20px" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form onSubmit={handleSubmit(apiLogin)}>
            <Controller
              name="email"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("email")}
                  value={value}
                  onChange={onChange}
                  error={!!errors.email}
                  helperText={errors.email ? t(errors.email.message) : ""}
                  style={{ width: "100%", marginTop: "20px" }}
                />
              )}
            />
            <Controller
              name="password"
              control={control}
              defaultValue=""
              render={({ field: { onChange, value } }) => (
                <TextField
                  label={t("password")}
                  type="password"
                  value={value}
                  onChange={onChange}
                  error={!!errors.password}
                  helperText={errors.password ? t(errors.password.message) : ""}
                  style={{ width: "100%", marginTop: "20px" }}
                />
              )}
            />
            <Button
              type="submit"
              variant="contained"
              style={{ width: "100%", marginTop: "20px" }}
            >
              {t("submit")}
            </Button>
          </form>
        </div>
      </Grid>
    </Grid>
  );
};

export default Login;
