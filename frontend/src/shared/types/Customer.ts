export interface Customer {
  email: string;
  phone_number: string;
  first_name: string;
  address: string;
  language: NonNullable<"EN" | "FR" | undefined>;
  last_name: string;
}
