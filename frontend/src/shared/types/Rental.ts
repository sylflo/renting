export interface Rental {
  uid: string;
  title: string;
  landlord_email: string;
  landlord_phone_number: string;
  landlord_first_name: string;
  landlord_last_name: string;
  landlord_address: string;
  rental_address: string;
  cleaning_price: number;
  deposit_percentage: number;
  security_deposit: number;
  checkin_weekday: number;
  checkin_time: any;
  checkout_weekday: number;
  checkout_time: any;
}

export type RentalFile = {
  filename: string;
  position: number;
  preview: Blob;
} & File;
