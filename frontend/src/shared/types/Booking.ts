import { Customer } from "./Customer";

export interface BookingFile {
  filename: string;
  position: number;
  get_path: string;
}

export interface Booking {
  rental_id: string;
  customer_email: string;
  created_at: string;
  booking_price: number;
  confirmation_email_sent: boolean;
  end_at: string;
  start_at: string;
  nights: number;
  total_person: number;
  total_adult: number;
  total_minor: number;
  tourist_tax: number;
  customer: Customer;
  files: [BookingFile];
  should_send_email: boolean;
  when_to_send_email: string;
}
