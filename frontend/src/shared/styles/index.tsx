import { makeStyles } from "tss-react/mui";

const useSharedStyles = makeStyles()(() => {
  return {
    link: {
      textDecoration: "none",
      color: "black",
    },
    fab: {
      position: "fixed",
      bottom: 10,
      right: 10,
      zIndex: 1,
    },
    active: {
      backgroundColor: "lightgrey",
    },
  };
});

export default useSharedStyles;
