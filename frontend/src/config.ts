/* eslint-disable */
import axios from "axios";

const instance = axios.create({
  baseURL:
    process.env.NODE_ENV === "production" ? "/api/" : process.env.REACT_APP_API,
});

const fetcher = (url: string) => instance.get(url).then((res: any) => res.data);

export { instance, fetcher };
