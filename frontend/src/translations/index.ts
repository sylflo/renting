import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import frenchTranslation from "./languages/fr";
import englishTranslation from "./languages/en";

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translation: {
        ...englishTranslation,
      },
    },
    fr: {
      translation: {
        ...frenchTranslation,
      },
    },
  },
  lng: "fr",
  fallbackLng: "en",
  interpolation: {
    escapeValue: false,
  },
});
