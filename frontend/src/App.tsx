/* eslint-disable no-underscore-dangle */
import React, { Suspense, ReactElement } from "react";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterLuxon } from "@mui/x-date-pickers/AdapterLuxon";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loading from "./components/Loading";
import Routess from "./router";
import "./translations/index";
import getSentrySdk from "./utils/sentry";
import setupAxiosInterceptors from "./utils/axios";

getSentrySdk();

const App = (): ReactElement => {
  setupAxiosInterceptors();

  return (
    <>
      <Suspense fallback={<Loading fullHeight />}>
        <LocalizationProvider dateAdapter={AdapterLuxon}>
          <Routess />
          <ToastContainer />
        </LocalizationProvider>
      </Suspense>
    </>
  );
};

export default App;
