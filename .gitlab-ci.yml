image: "registry.gitlab.com/sylflo/renting:backend-ci-latest"

cache: &js_cache
  key: ${CI_COMMIT_REF_SLUG}-node
  paths:
    - ./frontend/node_modules/
  policy: pull-push

.lint_stage:
  stage: lint
  except:
    - schedules

.postgres:
  services:
    - name: postgres:12.2-alpine
      alias: database
  variables:
    POSTGRES_DB: renting_test
    POSTGRES_USER: renting_test
    POSTGRES_PASSWORD: renting_test
    SQLALCHEMY_DATABASE_URL: "postgresql+asyncpg://renting_test:renting_test@database:5432/renting_test"
    SQLALCHEMY_DATABASE_URL_CLI: "postgresql://renting_test:renting_test@database:5432/renting_test"
    BRAND_NAME: "Best renting management ever"
    MAIL_USERNAME: "test@example.com"
    MAIL_PASSWORD: "aRandomPassword"
    MAIL_FROM: "myCompany@domain.com"
    MAIL_FROM_NAME: "my company name"
    MAIL_SERVER: "smtp.mail.com"
    FERNET_KEY: "v6vbzoGmALkWfOe0ctix6rmN4A0h2s96i0WY9_N2Syk="

stages:
  - lint
  - alembic
  - test
  - branch_build_docker
  - push_docker_tag
  - security
  - cache-image


#################
#     LINT      #
#################
flake8:
  extends: .lint_stage
  script:
    - flake8 --version
    - flake8 --max-line-length 88 ./backend/

black:
  extends: .lint_stage
  script:
    - black ./backend/

bandit:
  extends: .lint_stage
  script:
    - black --version
    - bandit -r ./backend/app -x ./backend/app/tests

isort:
  extends: .lint_stage
  script:
    - isort --version
    - cd backend
    - isort --check-only --profile black ./app
    - isort --check-only --profile black ./alembic

mypy:
  extends: .lint_stage
  script:
    - mypy --version
    - cd backend
    - poetry install --with dev
    - mypy ./app

eslint:
  extends: .lint_stage
  image: "node:22-alpine"
  cache:
   <<: *js_cache
  script:
    - cd frontend
    - yarn install
    - yarn run lint --version
    - yarn run lint
  allow_failure: true

prettier:
  extends: .lint_stage
  image: "node:22-alpine"
  cache:
   <<: *js_cache
  script:
    - cd frontend
    - yarn install
    - yarn run format --version
    - yarn run format

hadolint:
  extends: .lint_stage
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint --version
    - cd backend
    - hadolint Dockerfile
    - cd ../frontend
    - hadolint Dockerfile


#################
#    Alembic    #
#################
# TODO should be fixed
alembic:
  extends: .postgres
  stage: alembic
  allow_failure: true
  script:
    - alembic --version
    - cd backend
    - alembic upgrade head
  except:
    - schedules

##################
#   Unit tests   #
##################
unittest_backend:
  extends: .postgres
  stage: test
  script:
    - pytest --version
    - cd backend
    - poetry install --with dev 
    - PYTHONTRACEMALLOC=1 SQLALCHEMY_WARN_20=1 python -W always -m pytest -n auto --cov app/
    - coverage xml
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: ./backend/coverage.xml
    paths:
      - ./backend/ci_artifacts
      - ./backend/static/rentals
    expire_in: 1 week
  except:
    - schedules

#########################
# Docker build template #
#########################
.build:
  image: docker:20.10.8
  cache: []
  services:
    - docker:20.10.8-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
  except:
    - schedules

#############################################
# Build and push docker images for branches #
#############################################
build-docker-branch:
  extends: .build
  stage: branch_build_docker
  parallel:
    matrix:
      - FOLDER: backend
      - FOLDER: frontend
  script:
    - cd $FOLDER
    - docker pull $CI_REGISTRY_IMAGE:$FOLDER-$CI_COMMIT_REF_NAME || true
    - docker build --network host --cache-from $CI_REGISTRY_IMAGE:$FOLDER-$CI_COMMIT_REF_NAME --tag $CI_REGISTRY_IMAGE:$FOLDER-$CI_COMMIT_REF_NAME .
    - docker push $CI_REGISTRY_IMAGE:$FOLDER-$CI_COMMIT_REF_NAME


#############################################
# Build and push docker images when tagging #
#############################################
build-tag:
  extends: .build
  stage: push_docker_tag
  parallel:
    matrix:
      - TAG: backend
      - TAG: frontend
  script:
    - cd backend
    - docker pull $CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_REF_NAME $CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_REF_NAME $CI_REGISTRY_IMAGE:$TAG-latest
    - docker push $CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE:$TAG-latest
  only:
    - tags


#######################################
#               Safety                #
#       Scanning docker images        #
#            Trivy filesystem         #
#######################################

safety:
  stage: security
  script:
    - safety --version
    - cd backend
    - safety --version
    - safety check
  except:
    - schedules  

container_scanning:
  stage: security
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  variables:
    # No need to clone the repo, we exclusively work on artifacts.  See
    # https://docs.gitlab.com/ee/ci/runners/README.html#git-strategy
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
  cache:
    paths:
      - .trivycache/
  parallel:
    matrix:
      - TAG: backend
      - TAG: frontend
  script:
    - trivy --version --cache-dir .trivycache/
    - trivy clean --scan-cache && trivy image --download-db-only .trivycache/ --format template --template "@/contrib/gitlab.tpl"
    - trivy image --exit-code 0 "$CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_REF_NAME"
    - trivy image --exit-code 1 --severity CRITICAL "$CI_REGISTRY_IMAGE:$TAG-$CI_COMMIT_REF_NAME"
  except:
    - schedules  

fs_scanning:
  stage: security
  cache: []
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  parallel:
    matrix:
      - FOLDER: ./backend
      - FOLDER: ./frontend
  script:
    - trivy --version
    - trivy fs $FOLDER
  except:
    - schedules


#############################################
# Build and push docker cache images for ci #
#############################################
build-backend-cache-image-for-ci:
  extends: .build
  stage: cache-image
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:backend-ci-latest
  script:
    - cd backend
    - docker pull $IMAGE_TAG || true
    - docker build --network host --cache-from $IMAGE_TAG --tag $IMAGE_TAG -f Dockerfile.ci .
    - docker push $IMAGE_TAG
  only:
   - schedules
  except: []
