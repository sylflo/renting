from __future__ import annotations

import logging

import click
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app.main import settings
from app.models.user import User
from app.settings.auth import auth_handler

database_url = settings.SQLALCHEMY_DATABASE_URL_CLI
engine = create_engine(database_url)
session = Session(engine)
logging.basicConfig(level=logging.INFO)


@click.command()
@click.option("--email", prompt="Email")
@click.option("--password", prompt="Password")
def admin(email, password):
    hashed_password = auth_handler.encode_password(password)
    new_user = User(email=email, password=hashed_password, is_admin=True)
    try:
        session.add(new_user)
        session.commit()
        logging.info(f"Admin {email} has been created")
    except IntegrityError:
        session.rollback()
        logging.error(f"Admin {email} already exists!")

if __name__ == "__main__":
    admin()
