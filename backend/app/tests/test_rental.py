from __future__ import annotations

from pathlib import Path

from app.tests import client


def test_get_rental(rental_factory, rental_title, rental_email):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    response = client.get(
        f"/rentals/{rental_uid}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.json() == {
        "uid": rental_uid,
        "title": rental_title,
        "landlord_email": rental_email,
        "landlord_phone_number": "626-457-9788",
        "landlord_first_name": "John",
        "landlord_last_name": "Doe",
        "landlord_address": "Piazza del Duomo, 56126 Pisa PI, Italia",
        "rental_address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
        "cleaning_price": 100,
        "deposit_percentage": 30,
        "security_deposit": 500,
        "checkin_weekday": 5,
        "timezone": "Europe/Paris",
        "checkin_time": "19:00:00",
        "checkout_weekday": 5,
        "checkout_time": "06:00:00",
        "files": [],
        "duration_before_sending_email": 10,
    }


def test_upload_file_and_delete_to_rental(rental_factory, rental_title, rental_email):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    # Upload file
    filename = "./app/tests/images/image1.jpg"
    response = client.post(
        f"/rentals/{rental_uid}/files",
        headers={"Authorization": f"Bearer {access_token}"},
        files={"files": ("image1", open(filename, "rb"), "image/jpeg")},
    )
    file = Path(f"static/rentals/{rental_uid}/files/image1")
    assert file.exists() is True
    assert response.json() == {"filenames": ["image1"]}
    # Delete file
    response = client.delete(
        f"/rentals/{rental_uid}/files/image1",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert file.exists() is False
    assert response.status_code == 204
    response = client.delete(
        f"/rentals/{rental_uid}/files/image1",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 404


def test_delete_rental_with_bookings(
    rental_factory, booking_factory, rental_title, rental_email
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    booking_factory(access_token, rental_uid, "2022-09-07", "2022-09-27")
    response = client.delete(
        f"/rentals/{rental_uid}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 403
    assert response.json() == {"detail": f"Rental {rental_uid} has bookings"}
