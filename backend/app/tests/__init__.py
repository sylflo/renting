from __future__ import annotations

from faker import Faker
from fastapi.testclient import TestClient
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from app.main import app, fm, get_async_db

SQLALCHEMY_DATABASE_URL = (
    "postgresql+asyncpg://renting_test:renting_test@database:5432/renting_test"
)


engine = create_async_engine(SQLALCHEMY_DATABASE_URL, poolclass=NullPool)
TestingAsyncSessionLocal = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)  # type: ignore


async def override_get_async_db():
    try:
        db = TestingAsyncSessionLocal()
        yield db
    finally:
        await db.close()


app.dependency_overrides[get_async_db] = override_get_async_db
client = TestClient(app)
fm.config.SUPPRESS_SEND = 1

fake = Faker()
