from __future__ import annotations

import os
import uuid
from pathlib import Path

import pytest

from app.tests import client
from app.utils.booking import calculate_tourist_tax

BOOKING_START = "2021-08-07"
BOOKING_END = "2021-08-27"


@pytest.mark.parametrize(
    "booking_start, booking_end, language",
    [(BOOKING_START, BOOKING_END, "en"), ("2022-08-07", "2022-08-27", "fr")],
)
def test_create_booking(
    rental_factory, rental_title, rental_email, booking_start, booking_end, language
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    response = client.post(
        f"/rentals/{rental_uid}/bookings",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "email": "john.doe@customer.com",
            "phone_number": "+33981287293",
            "language": "EN",
            "first_name": "John",
            "last_name": "Doe",
            "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "start_at": booking_start,
            "end_at": booking_end,
            "booking_price": 500,
            "total_adult": 2,
            "total_minor": 0,
            "tourist_tax": True,
        },
    )
    assert response.status_code == 201
    assert response.json()["start_at"] == booking_start
    assert response.json()["end_at"] == booking_end
    assert response.json()["booking_price"] == 500
    assert response.json()["tourist_tax"] == 22
    assert response.json()["total_person"] == 2
    assert response.json()["nights"] == 21
    assert response.json()["customer"] == {
        "email": "john.doe@customer.com",
        "phone_number": "+33981287293",
        "language": "EN",
        "first_name": "John",
        "last_name": "Doe",
        "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
    }


def test_create_two_bookings(
    booking_factory, rental_factory, rental_title, rental_email
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    booking_factory(access_token, rental_uid, "2100-08-13", "2100-08-20")
    booking_factory(access_token, rental_uid, "2100-08-13", "2100-08-20")


def test_create_booking_fails(
    rental_factory, rental_title, rental_email, booking_factory
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    booking_factory(access_token, rental_uid, "2021-08-13", "2021-08-20")
    response = client.post(
        f"/rentals/{rental_uid}/bookings",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "email": "john@doe.com",
            "phone_number": "+33981287293",
            "language": "EN",
            "first_name": "John",
            "last_name": "Doe",
            "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "start_at": "2021-08-14",
            "end_at": "2021-08-20",
            "booking_price": 500,
            "total_adult": 1,
            "total_minor": 0,
            "tourist_tax": True,
        },
    )
    assert (
        response.json()["detail"]
        == "Dates from 2021-08-14 to 2021-08-20 are already booked"
    )
    assert response.status_code == 403


@pytest.mark.xfail
def test_send_confirmation_email_booking_does_not_exist(
    rental_factory, rental_title, rental_email
):
    access_token, _ = rental_factory(rental_title, rental_email)
    uid = uuid.uuid4()
    response = client.post(
        f"/rentals/{uid}/bookings/2021-08-14/2021-08-25/emails/confirmation",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Booking does not exist"


@pytest.mark.xfail
def test_send_confirmation_email_again(
    rental_factory, rental_title, rental_email, booking_factory
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    booking_factory(access_token, rental_uid, "2021-08-07", "2021-08-27")
    response = client.post(
        f"/rentals/{rental_uid}" "/bookings/2021-08-07/2021-08-27/emails/confirmation",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 403
    assert response.json()["detail"] == "Confirmation email already sent"


def test_calculate_tourist_tax():
    result = calculate_tourist_tax(700, 1, 7)
    assert result == 19
    result = calculate_tourist_tax(700, 2, 7)
    assert result == 31
    result = calculate_tourist_tax(560, 2, 7)
    assert result == 25


def test_booking_from_another_rental_same_date_as_previous_one(
    rental_factory, rental_title, rental_email
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    # Create rental
    response = client.post(
        "/rentals",
        headers={"Authorization": f"Bearer {access_token}"},
        json={"title": "new title"},
    )
    rental_uid = response.json()["uid"]
    assert response.json()["title"] == "new title"

    # Update rental
    response = client.put(
        f"/rentals/{rental_uid}",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "title": "Title for second rental",
            "landlord_email": "john@doe.com",
            "landlord_phone_number": "626-457-9788",
            "landlord_first_name": "John",
            "landlord_last_name": "Doe",
            "landlord_address": "Piazza del Duomo, 56126 Pisa PI, Italia",
            "rental_address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "cleaning_price": 100,
            "deposit_percentage": 30,
            "security_deposit": 500,
            "timezone": "Europe/Paris",
            "checkin_weekday": 5,
            "checkin_time": "19:00:00",
            "checkout_weekday": 5,
            "checkout_time": "06:00:00",
            "tourist_tax": True,
            "duration_before_sending_email": 20,
        },
    )

    # Book same date as previous rental
    response = client.post(
        f"/rentals/{rental_uid}/bookings",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "email": "john.doe@customer.com",
            "phone_number": "+33981287293",
            "language": "EN",
            "first_name": "John",
            "last_name": "Doe",
            "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "start_at": "2021-08-07",
            "end_at": "2021-08-27",
            "booking_price": 500,
            "total_adult": 1,
            "total_minor": 1,
            "tourist_tax": True,
        },
    )
    assert response.status_code == 201


@pytest.mark.parametrize(
    "booking_start, booking_end",
    [(BOOKING_START, BOOKING_END), ("2022-08-07", "2022-08-27")],
)
def test_booking_delete(
    rental_factory,
    booking_factory,
    rental_title,
    rental_email,
    booking_start,
    booking_end,
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    booking_factory(access_token, rental_uid, booking_start, booking_end)
    contract = Path(
        f"static/rentals/{rental_uid}/" f"bookings/{booking_start}/contract_en.pdf"
    )
    folder = Path(f"static/rentals/{rental_uid}/bookings/{booking_start}")
    assert contract.exists() is True
    assert folder.exists() is True

    response = client.delete(
        f"/rentals/{rental_uid}/bookings/" f"{booking_start}/{booking_end}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 204

    assert contract.exists() is False
    assert folder.exists() is False


def test_get_bookings(booking_factory, rental_factory, rental_title, rental_email):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    [
        booking_factory(access_token, rental_uid, f"2100-0{i}-13", f"2100-0{i}-20")
        for i in range(1, 10)
    ]
    [
        booking_factory(access_token, rental_uid, f"2101-0{i}-13", f"2101-0{i}-20")
        for i in range(1, 10)
    ]
    for i in range(1):
        response = client.get(
            f"/rental/{rental_uid}/bookings?skip={i}&limit=10",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        assert len(response.json()["bookings"]) == 10


@pytest.mark.parametrize("cleaning", [True, False])
def test_create_booking_with_cleaning_option(
    rental_factory, rental_title, rental_email, cleaning
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    response = client.post(
        f"/rentals/{rental_uid}/bookings",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "email": "john.doe@customer.com",
            "phone_number": "+33981287293",
            "language": "EN",
            "first_name": "John",
            "last_name": "Doe",
            "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "start_at": "2023-07-10",
            "end_at": "2023-07-15",
            "booking_price": 500,
            "total_adult": 2,
            "total_minor": 0,
            "tourist_tax": True,
            "cleaning": cleaning,
        },
    )
    if cleaning:
        assert response.json()["cleaning_price"] == 100
    else:
        assert response.json()["cleaning_price"] == 0


@pytest.mark.parametrize("language", ["EN", "FR"])
def test_create_booking_with_different_language(
    rental_factory, rental_title, rental_email, language
):
    access_token, rental_uid = rental_factory(rental_title, rental_email)
    client.post(
        f"/rentals/{rental_uid}/bookings",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "email": "john.doe@customer.com",
            "phone_number": "+33981287293",
            "language": language,
            "first_name": "John",
            "last_name": "Doe",
            "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
            "start_at": "2023-07-10",
            "end_at": "2023-07-15",
            "booking_price": 500,
            "total_adult": 2,
            "total_minor": 0,
            "tourist_tax": True,
        },
    )

    dir_path = f"./static/rentals/{rental_uid}/bookings/2023-07-10"
    filename = f"{dir_path}/contract_{language.lower()}.pdf"
    assert os.path.isfile(filename)
