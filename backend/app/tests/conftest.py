from __future__ import annotations

import asyncio
import json

import pytest
import pytest_asyncio
from filelock import FileLock

from app.api.auth import auth_handler
from app.database.base import Base
from app.database.database import AsyncSessionLocal
from app.models.user import User

from . import client, engine, fake


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


async def _create_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@pytest_asyncio.fixture(scope="session")
async def create_db(tmp_path_factory, worker_id, name="_create_db"):
    if not worker_id:
        # not executing in with multiple workers, just produce the data and let
        # pytest's fixture caching do its job
        await _create_db()

    # get the temp directory shared by all workers
    root_tmp_dir = tmp_path_factory.getbasetemp().parent

    fn = root_tmp_dir / "data.json"
    with FileLock(str(fn) + ".lock"):
        if fn.is_file():
            data = json.loads(fn.read_text())
        else:
            data = await _create_db()
            fn.write_text(json.dumps(data))
    return data


@pytest_asyncio.fixture()
async def access_token(create_db):
    email = fake.email()
    password = fake.password()
    async with AsyncSessionLocal() as session:
        async with session.begin():
            # Create user
            hashed_password = auth_handler.encode_password(password)
            new_user = User(email=email, password=hashed_password)
            session.add(new_user)
            await session.commit()
    response = client.post(
        "/login",
        json={"email": email, "password": password},
    )
    return response.json()["tokens"]["access_token"]


@pytest.fixture()
def rental_title():
    return fake.language_name()


@pytest.fixture()
def rental_email():
    return fake.email()


@pytest.fixture()
def rental_factory(access_token):
    def create(title: str, email: str):
        response = client.post(
            "/rentals",
            headers={"Authorization": f"Bearer {access_token}"},
            json={"title": "Title", "email": "john.doe@landlord.com"},
        )
        uid = response.json()["uid"]
        client.put(
            f"/rentals/{uid}",
            headers={"Authorization": f"Bearer {access_token}"},
            json={
                "title": title,
                "landlord_email": email,
                "landlord_phone_number": "626-457-9788",
                "landlord_first_name": "John",
                "landlord_last_name": "Doe",
                "landlord_address": "Piazza del Duomo, 56126 Pisa PI, Italia",
                "rental_address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
                "cleaning_price": 100,
                "deposit_percentage": 30,
                "security_deposit": 500,
                "timezone": "Europe/Paris",
                "checkin_weekday": 5,
                "checkin_time": "19:00",
                "checkout_weekday": 5,
                "checkout_time": "06:00",
                "duration_before_sending_email": 10,
            },
        )
        return access_token, uid

    return create


@pytest.fixture()
def booking_factory():
    def create(access_token: str, rental_uid: str, start_at, end_at: str):
        response = client.post(
            f"/rentals/{rental_uid}/bookings",
            headers={"Authorization": f"Bearer {access_token}"},
            json={
                "email": "john.doe@customer.com",
                "phone_number": "+33981287293",
                "language": "EN",
                "first_name": "John",
                "last_name": "Doe",
                "address": "Champ de Mars, 5 Avenue Anatole France, 75007 Paris",
                "start_at": start_at,
                "end_at": end_at,
                "booking_price": 500,
                "total_adult": 2,
                "total_minor": 0,
                "tourist_tax": False,
            },
        )
        return response.json()

    return create
