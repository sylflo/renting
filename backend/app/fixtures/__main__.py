from __future__ import annotations

import factory
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from app.models.booking import EnumLanguage, RentalBooking, RentalCustomer
from app.models.rental import EnumWeek, Rental
from app.models.user import User
from app.settings.auth import auth_handler

# mypy: ignore-errors


engine = create_engine("postgresql://renting:renting@0.0.0.0:5432/renting")
session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = User
        sqlalchemy_session = session

    email = "admin@admin.fr"
    password = auth_handler.encode_password("admin")
    is_admin = True


class RentalFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Rental
        sqlalchemy_session = session

    uid = factory.Faker("uuid4")
    title = factory.Faker("sentence")
    landlord_email = factory.Faker("email")
    landlord_phone_number = factory.Faker("phone_number")
    landlord_first_name = factory.Faker("first_name")
    landlord_last_name = factory.Faker("last_name")
    landlord_address = factory.Faker("address")
    rental_address = factory.Faker("address")
    cleaning_price = factory.Faker("random_int")
    deposit_percentage = factory.Faker("random_int", min=0, max=100)
    security_deposit = factory.Faker("random_int")
    checkin_weekday = EnumWeek.SATURDAY
    checkin_time = factory.Faker("date_time")
    checkout_weekday = EnumWeek.SATURDAY
    checkout_time = factory.Faker("date_time")


class RentalCustomerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RentalCustomer
        sqlalchemy_session = session

    rental_id = ""
    email = factory.Faker("email")
    phone_number = factory.Faker("phone_number")
    language = EnumLanguage.EN
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    address = factory.Faker("address")


class RentalBookingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RentalBooking
        sqlalchemy_session = session

    rental_id = ""
    start_at = factory.Faker("date")
    end_at = factory.Faker("date")
    customer_email = ""
    nights = factory.Faker("random_int")
    total_person = factory.Faker("random_int")
    booking_price = factory.Faker("random_int")
    deposit = factory.Faker("random_int")
    total_price = factory.Faker("random_int")
    tourist_tax = factory.Faker("random_int")
    confirmation_email_sent = factory.Faker("boolean")


# Create an admin User
UserFactory()

# Create a Rental
RentalFactory()
rental = session.query(Rental).first()

# Create a Customer
RentalCustomerFactory(rental_id=rental.uid)
customer = session.query(RentalCustomer).first()

# Create a Booking batch
RentalBookingFactory.create_batch(
    size=100, rental_id=rental.uid, customer_email=customer.email
)

# Populate the database
session.commit()

# print("===Fixtures created===")
