from __future__ import annotations

import asyncio
import glob
import json
import logging
from contextlib import asynccontextmanager
from datetime import date
from os import getcwd

import sentry_sdk
from fastapi import Depends, FastAPI
from fastapi.logger import logger
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_mail import ConnectionConfig, FastMail
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from sqlalchemy import and_
from sqlalchemy.future import select

from app.config import Settings, get_settings, settings
from app.database.database import AsyncSessionLocal
from app.settings.auth import verify_access_token

templates = Jinja2Templates(directory="templates")

# Languages
default_fallback = "en"
languages = {}
language_list = glob.glob("languages/*.json")
for lang in language_list:
    filename = lang.split("/")
    lang_code = filename[1].split(".")[0]
    with open(lang, "r", encoding="utf8") as file:  # type: ignore
        languages[lang_code] = json.load(file)

# Email config using the cached settings instance
email_conf = ConnectionConfig(
    MAIL_USERNAME=settings.MAIL_USERNAME,
    MAIL_PASSWORD=settings.MAIL_PASSWORD.get_secret_value(),
    MAIL_FROM=settings.MAIL_FROM,
    MAIL_PORT=settings.MAIL_PORT,
    MAIL_SERVER=settings.MAIL_SERVER,
    MAIL_FROM_NAME=settings.MAIL_FROM_NAME,
    MAIL_STARTTLS=True,
    MAIL_SSL_TLS=False,
    USE_CREDENTIALS=True,
    VALIDATE_CERTS=settings.MAIL_VALIDATE_CERTS,
    TEMPLATE_FOLDER=f"{getcwd()}/templates/emails",
)
fm = FastMail(email_conf)

# Logging config
gunicorn_logger = logging.getLogger("gunicorn.error")
logger.handlers = gunicorn_logger.handlers
if __name__ != "__main__":
    logger.setLevel(gunicorn_logger.level)
else:
    logger.setLevel(logging.DEBUG)


async def send_contract_to_customers() -> None:
    from app.models.booking import RentalBooking
    from app.utils.booking import send_booking_confirmation_email

    processing_lock = asyncio.Lock()  # Prevent overlapping executions
    while True:
        async with processing_lock:
            async with AsyncSessionLocal() as session:
                bookings = await session.execute(
                    select(RentalBooking).where(
                        and_(
                            date.today() >= RentalBooking.when_to_send_email,
                            RentalBooking.should_send_email.is_(True),
                            RentalBooking.confirmation_email_sent.is_(False),
                        )
                    )
                )

                booking_list = bookings.scalars().all()
                if booking_list:
                    logger.info(f"Found {len(booking_list)} bookings to process.")
                else:
                    logger.info("No bookings found that require emails.")
                for booking in booking_list:
                    logger.debug(
                        f"Sending email for booking: {booking.start_at} - {booking.end_at}"  # noqa: E501
                    )
                    await send_booking_confirmation_email(booking)
        # await asyncio.sleep(60 * 60)  # One hour
        await asyncio.sleep(10)  # One hour


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Lifespan event handler replacing on_event('startup')"""
    loop = asyncio.get_event_loop()
    loop.create_task(send_contract_to_customers())  # Start background task
    yield


# Init server
app = FastAPI(
    title="Renting management",
    version="0.0.1",
    lifespan=lifespan,
)

# Sentry configuration using the cached settings
if settings.SENTRY_API == "":
    logger.warning("Error will not be reported to Sentry")
else:
    sentry_sdk.init(dsn=settings.SENTRY_API)
    asgi_app = SentryAsgiMiddleware(app)

# CORS middleware using the cached settings
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Dependencies
async def get_async_db():
    async with AsyncSessionLocal() as session:
        async with session.begin():
            yield session


@app.get("/sentry-admin/")
def read_sentry_admin(
    settings: Settings = Depends(get_settings), token=Depends(verify_access_token)
):
    return {"SENTRY_ADMIN": settings.SENTRY_ADMIN}


app.mount("/static", StaticFiles(directory="static"), name="static")


# API routes
from app.api.auth import *  # noqa: E402, F403, F401
from app.api.booking import *  # noqa: E402, F403, F401
from app.api.rental import *  # noqa: E402, F403, F401
from app.api.timezones import *  # noqa: E402, F403, F401
