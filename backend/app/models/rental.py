from __future__ import annotations

import enum
from datetime import time

from fastapi import HTTPException
from sqlalchemy import Enum, ForeignKey, Integer, String, Time
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.database.base import Base
from app.models.booking import RentalCustomer


async def get_rental_from_uid(rental_uid: str, db: AsyncSession):
    """
    Retrieve a rental by its UID.

    Args:
        rental_uid (str): The UID of the rental.
        db (AsyncSession): The database session.

    Raises:
        HTTPException: If the rental does not exist.

    Returns:
        Rental: The rental object.
    """
    db_rental = await db.get(Rental, rental_uid)
    if db_rental is None:
        raise HTTPException(
            status_code=404,
            detail=f"Rental with uid {rental_uid} does not exist",
        )
    return db_rental


class EnumWeek(enum.Enum):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6


class RentalFile(Base):
    __tablename__ = "rental_file"

    rental_id: Mapped[str] = mapped_column(
        String(36), ForeignKey("rental.uid"), primary_key=True
    )
    filename: Mapped[str] = mapped_column(String(255), nullable=False, primary_key=True)
    position: Mapped[int]

    rental: Mapped[Rental] = relationship(
        "Rental",
        back_populates="files"
    )

    def get_path(self):
        return f"./static/rentals/{self.rental_id}/files/{self.filename}"

    def __repr__(self) -> str:
        return f"<RentalFile(rental_id={self.rental_id!r}, filename={self.filename!r})>"


class Rental(Base):
    __tablename__ = "rental"

    uid: Mapped[str] = mapped_column(
        String(36), primary_key=True, nullable=False, unique=True, index=True
    )
    title: Mapped[str] = mapped_column(String(255), nullable=False)
    landlord_email: Mapped[str] = mapped_column(String(255), nullable=True)
    landlord_phone_number: Mapped[str] = mapped_column(String(50), nullable=True)
    landlord_first_name: Mapped[str] = mapped_column(String(50), nullable=True)
    landlord_last_name: Mapped[str] = mapped_column(String(50), nullable=True)
    landlord_address: Mapped[str] = mapped_column(String(255), nullable=True)
    rental_address: Mapped[str] = mapped_column(String(255), nullable=True)
    cleaning_price: Mapped[int] = mapped_column(Integer, nullable=True)
    deposit_percentage: Mapped[int] = mapped_column(Integer, nullable=True)
    security_deposit: Mapped[int] = mapped_column(Integer, nullable=True)
    timezone: Mapped[str] = mapped_column(String(255), nullable=True)
    checkin_weekday: Mapped[EnumWeek] = mapped_column(Enum(EnumWeek), nullable=True)
    checkin_time: Mapped[time] = mapped_column(Time, nullable=True)
    checkout_weekday: Mapped[EnumWeek] = mapped_column(Enum(EnumWeek), nullable=True)
    checkout_time: Mapped[time] = mapped_column(Time, nullable=True)
    duration_before_sending_email: Mapped[int] = mapped_column(Integer, nullable=True)  # in seconds # noqa: E501

    files: Mapped[list[RentalFile]] = relationship(
        "RentalFile",
        back_populates="rental",
        cascade="all, delete, delete-orphan",
        order_by="RentalFile.position",
        lazy="subquery",
    )
    customers: Mapped[list[RentalCustomer]] = relationship(
        "RentalCustomer",
        cascade="all, delete, delete-orphan",
        back_populates="rental",
        lazy="subquery",
    )

    def __repr__(self) -> str:
        return f"<Rental(uid={self.uid!r}, title={self.title!r})>"
