from __future__ import annotations

import enum
from datetime import date, datetime, timedelta, timezone
from typing import TYPE_CHECKING, List

from fastapi import HTTPException
from sqlalchemy import (
    Boolean,
    Date,
    DateTime,
    Enum,
    ForeignKey,
    ForeignKeyConstraint,
    String,
)
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import func

from app.database.base import Base

if TYPE_CHECKING:
    from app.models.rental import Rental


async def get_booking_by_rental_and_dates(
    rental_uid: str, start_at: date, end_at: date, db: AsyncSession
):
    """
    Retrieve a booking for a given rental using the rental UID and date range.

    Args:
        rental_uid (str): The UID of the rental.
        start_at (date): The start date of the booking.
        end_at (date): The end date of the booking.
        db (AsyncSession): The database session.

    Raises:
        HTTPException: If the booking does not exist.

    Returns:
        RentalBooking: The matching booking object.
    """
    db_booking = await db.get(RentalBooking, (rental_uid, start_at, end_at))
    if db_booking is None:
        raise HTTPException(
            status_code=404,
            detail="Booking does not exist",
        )
    return db_booking


async def find_booking_from_uid(
    rental_uid: str, db: AsyncSession
) -> List[RentalBooking]:
    """
    Find all bookings associated with a rental UID.

    Args:
        rental_uid (str): The UID of the rental.
        db (AsyncSession): The database session.

    Returns:
        List[RentalBooking]: A list of booking objects for the rental.
    """
    bookings = await db.execute(
        select(RentalBooking).filter(RentalBooking.rental_id == rental_uid)
    )
    return list(bookings.scalars().all())


class EnumLanguage(str, enum.Enum):
    EN = "EN"
    FR = "FR"


class RentalCustomer(Base):
    __tablename__ = "rental_customer"

    rental_id: Mapped[str] = mapped_column(
        String(36), ForeignKey("rental.uid"), primary_key=True, nullable=False
    )
    email: Mapped[str] = mapped_column(String(255), primary_key=True, nullable=False)
    phone_number: Mapped[str] = mapped_column(String(100), nullable=False)
    language: Mapped[EnumLanguage] = mapped_column(
        Enum(EnumLanguage),
        default=EnumLanguage.EN,
        nullable=False,
    )
    first_name: Mapped[str] = mapped_column(String(50), nullable=False)
    last_name: Mapped[str] = mapped_column(String(50), nullable=False)
    address: Mapped[str] = mapped_column(String(255), nullable=False)

    rental: Mapped[Rental] = relationship(
        "Rental", back_populates="customers", lazy="subquery"
    )

    def __repr__(self) -> str:
        return f"<RentalCustomer(email={self.email!r}, rental_id={self.rental_id!r})>"


class RentalBooking(Base):
    __tablename__ = "rental_booking"

    rental_id: Mapped[str] = mapped_column(
        String(36),
        primary_key=True,
        nullable=False,
    )
    start_at: Mapped[date] = mapped_column(Date, nullable=False, primary_key=True)
    end_at: Mapped[date] = mapped_column(Date, nullable=False, primary_key=True)
    customer_email: Mapped[str] = mapped_column(String(255), nullable=False)
    nights: Mapped[int]
    created_at: Mapped[date] = mapped_column(
        DateTime,
        default=lambda: datetime.now(timezone.utc).replace(tzinfo=None),
        server_default=func.now()
    )
    total_adult: Mapped[int]
    total_minor: Mapped[int]
    booking_price: Mapped[int]
    total_price: Mapped[int]
    tourist_tax: Mapped[float]
    cleaning_price: Mapped[float]
    deposit: Mapped[float]
    should_send_email: Mapped[bool] = mapped_column(
        Boolean, default=True, nullable=False
    )
    confirmation_email_sent: Mapped[bool] = mapped_column(
        Boolean, default=False, nullable=False
    )
    when_to_send_email: Mapped[date] = mapped_column(Date, nullable=True)

    @property
    def total_person(self) -> int:
        return self.total_adult + self.total_minor

    @property
    def contract_file_test(self):
        return f"./templates/pdf_contract/contract_{self.customer.language.lower()}.pdf"  # noqa: E501

    customer: Mapped[RentalCustomer] = relationship(
        "RentalCustomer",
        lazy="subquery",
    )
    dates: Mapped[list[RentalBookingDate]] = relationship(
        "RentalBookingDate",
        cascade="all,delete,delete-orphan",
        back_populates="booking",
    )
    files: Mapped[list[RentalBookingFile]] = relationship(
        "RentalBookingFile",
        cascade="all,delete,delete-orphan",
        back_populates="booking",
        lazy="subquery",
    )

    __table_args__ = (
        ForeignKeyConstraint(
            [rental_id, customer_email],  # type: ignore
            [
                RentalCustomer.rental_id,
                RentalCustomer.email,
            ],
        ),
        {},
    )

    def get_end_at_customer_repr(self):
        return self.end_at + timedelta(days=1)

    def __repr__(self) -> str:
        return (
            f"<RentalBooking(rental_id={self.rental_id!r}, start_at={self.start_at!r}, "
            f"end_at={self.end_at!r}, customer_email={self.customer_email!r})>"
        )


class RentalBookingFile(Base):
    __tablename__ = "rental_booking_file"

    rental_id: Mapped[str] = mapped_column(String(36), nullable=False, primary_key=True)
    filename: Mapped[str] = mapped_column(String(255), nullable=False, primary_key=True)
    booking_start_at: Mapped[date] = mapped_column(
        Date, nullable=False, primary_key=True
    )
    booking_end_at: Mapped[date] = mapped_column(Date, nullable=False)
    position: Mapped[int]

    booking: Mapped[RentalBooking] = relationship(
        "RentalBooking", back_populates="files"
    )

    __table_args__ = (
        ForeignKeyConstraint(
            [rental_id, booking_start_at, booking_end_at],  # type: ignore
            [
                RentalBooking.rental_id,
                RentalBooking.start_at,
                RentalBooking.end_at,
            ],
        ),
        {},
    )

    @property
    def get_path(self):
        return f"static/rentals/{self.rental_id}/bookings/{self.booking_start_at}/{self.filename}"  # noqa: E501

    def __repr__(self) -> str:
        return (
            f"<RentalBookingFile(rental_id={self.rental_id!r}, filename={self.filename!r}, "  # noqa: E501
            f"booking_start_at={self.booking_start_at!r})>"
        )


class RentalBookingDate(Base):
    __tablename__ = "rental_booking_date"

    rental_id: Mapped[str] = mapped_column(String(36), nullable=False, primary_key=True)
    date: Mapped[date] = mapped_column(Date, nullable=False, primary_key=True)
    booking_start_at: Mapped[date] = mapped_column(Date, nullable=False)  # type: ignore
    booking_end_at: Mapped[date] = mapped_column(Date, nullable=False)  # type: ignore

    booking: Mapped[RentalBooking] = relationship(
        "RentalBooking", back_populates="dates"
    )
    __table_args__ = (
        ForeignKeyConstraint(
            [rental_id, booking_start_at, booking_end_at],  # type: ignore
            [
                RentalBooking.rental_id,
                RentalBooking.start_at,
                RentalBooking.end_at,
            ],
        ),
        {},
    )

    def __repr__(self) -> str:
        return f"<RentalBookingDate(rental_id={self.rental_id!r}, date={self.date!r})>"
