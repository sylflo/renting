from __future__ import annotations

from typing import Optional

from fastapi import HTTPException
from sqlalchemy import Boolean, String
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Mapped, mapped_column

from app.database.base import Base


async def get_user_by_email(email: str, db: AsyncSession) -> User:
    """
    Retrieve a user by their email.

    Args:
        email (str): The email of the user.
        db (AsyncSession): The database session.

    Raises:
        HTTPException: If the user is not found.

    Returns:
        User: The user object.
    """
    db_user: Optional[User] = await db.get(User, email)
    if db_user is None:
        raise HTTPException(status_code=404, detail=f"User {email} not found")
    return db_user


class User(Base):
    __tablename__ = "rental_user"

    email: Mapped[str] = mapped_column(
        String(255), primary_key=True, nullable=False, unique=True, index=True
    )
    password: Mapped[str] = mapped_column(String(255), nullable=False)
    is_admin: Mapped[Boolean] = mapped_column(Boolean, default=False, nullable=False)

    def __repr__(self) -> str:
        return f"<User(email={self.email!r}, is_admin={self.is_admin!r})>"
