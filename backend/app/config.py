from __future__ import annotations

from functools import lru_cache
from typing import List, Optional

from pydantic import SecretStr
from pydantic_settings import BaseSettings, SettingsConfigDict


# Server settings
class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")

    SQLALCHEMY_DATABASE_URL: str
    SQLALCHEMY_DATABASE_URL_CLI: str
    CORS_ORIGINS: List[str] = ["http://localhost:3000"]
    JWT_SECRET_KEY: SecretStr
    ACCESS_TOKEN_DURATION: int = 30  # in days
    MAIL_VALIDATE_CERTS: Optional[bool] = True
    MAIL_USERNAME: str
    MAIL_PASSWORD: SecretStr
    MAIL_FROM: str
    MAIL_SERVER: str
    MAIL_PORT: Optional[int] = 587
    MAIL_FROM_NAME: str
    MAIL_BBC: Optional[str] = None
    SENTRY_API: Optional[str] = None
    SENTRY_ADMIN: Optional[str] = None
    ALLOWED_EXTENSIONS: List[str] = [
        "image/png",
        "image/jpeg",
        "application/pdf",
        "image/webp",
    ]
    MAX_UPLOAD_FILES: int = 5


# Cache the settings instance so it's only created once.
@lru_cache()
def get_settings() -> Settings:
    return Settings()


# Instantiate settings once and reuse it
settings = get_settings()
