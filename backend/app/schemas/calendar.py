from __future__ import annotations

from datetime import date

from pydantic import BaseModel


class DateRange(BaseModel):
    start_at: date
    end_at: date
