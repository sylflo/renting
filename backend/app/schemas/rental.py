from __future__ import annotations

from datetime import time
from typing import Dict, List, Optional

from pydantic import BaseModel, EmailStr, Field, NonNegativeInt, RootModel

from app.models.rental import EnumWeek


class RentalFile(BaseModel):
    filename: str
    position: int

    class ConfigDict:
        from_attributes = True


FileDict = RootModel[Dict[str, RentalFile]]


class RentalBase(BaseModel):
    title: str
    landlord_email: Optional[EmailStr] = None
    landlord_phone_number: Optional[str] = None
    landlord_first_name: Optional[str] = None
    landlord_last_name: Optional[str] = None
    landlord_address: Optional[str] = None
    rental_address: Optional[str] = None
    cleaning_price: Optional[NonNegativeInt] = None
    deposit_percentage: Optional[int] = Field(ge=0, le=100)
    security_deposit: Optional[NonNegativeInt] = None
    timezone: Optional[str] = None
    checkin_weekday: Optional[EnumWeek] = None
    checkin_time: Optional[time] = None
    checkout_weekday: Optional[EnumWeek] = None
    checkout_time: Optional[time] = None
    duration_before_sending_email: Optional[int] = None

    class ConfigDict:
        from_attributes = True


class RentalCreate(BaseModel):
    title: str


class RentalUpdate(RentalBase):
    landlord_email: EmailStr
    landlord_phone_number: str
    landlord_first_name: str
    landlord_last_name: str
    landlord_address: str
    rental_address: str
    cleaning_price: NonNegativeInt
    deposit_percentage: int = Field(ge=0, le=100)
    security_deposit: NonNegativeInt
    timezone: str
    checkin_weekday: EnumWeek
    checkin_time: time
    checkout_weekday: EnumWeek
    checkout_time: time
    duration_before_sending_email: int = Field(ge=0)


class Rental(RentalBase):
    uid: str
    files: List[RentalFile]
