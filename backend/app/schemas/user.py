from __future__ import annotations

from pydantic import BaseModel, EmailStr


class Auth(BaseModel):
    email: EmailStr
    password: str
