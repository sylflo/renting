from __future__ import annotations

from datetime import date
from typing import Dict, List, Optional

from pydantic import BaseModel, EmailStr, RootModel

from app.models.booking import EnumLanguage


class CustomerBase(BaseModel):
    email: EmailStr
    phone_number: str
    first_name: str
    last_name: str
    address: str
    language: EnumLanguage

    class ConfigDict:
        from_attributes = True


class CustomerCreate(CustomerBase):
    pass


class Customer(CustomerBase):
    pass


CustomerDict = RootModel[Dict[str, Customer]]


class BookingFile(BaseModel):
    filename: str
    position: int
    get_path: str

    class ConfigDict:
        from_attributes = True


class BookingBase(BaseModel):
    start_at: date
    end_at: date
    booking_price: int
    total_adult: int
    total_minor: int
    should_send_email: Optional[bool] = True
    confirmation_email_sent: Optional[bool] = False
    when_to_send_email: Optional[date] = None


class BookingCreate(BookingBase, CustomerBase):
    tourist_tax: Optional[bool] = True
    cleaning: Optional[bool] = True

    class ConfigDict:
        from_attributes = True


class Booking(BookingBase):
    customer: Customer
    total_person: int
    nights: int
    tourist_tax: float
    cleaning_price: float
    files: List[BookingFile]

    class ConfigDict:
        from_attributes = True


class BookingList(BaseModel):
    pages: int
    bookings: List[Booking]
