from __future__ import annotations

from datetime import date
from decimal import ROUND_DOWN, Decimal, getcontext
from math import ceil
from os.path import basename
from pathlib import Path

from fastapi_mail import MessageSchema
from weasyprint import HTML  # type: ignore

from app.config import settings
from app.main import fm, languages, templates
from app.models.booking import EnumLanguage, RentalBooking, RentalBookingFile


def calculate_tourist_tax(booking_price: int, people_nb: int, nights_nb: int) -> int:
    getcontext().rounding = ROUND_DOWN
    tourist_tax = 4  # in percentage
    department_tax = 10  # in percentage
    max_tourist_tax = Decimal(2.41)  # in euros
    # Get night price
    night_price = Decimal(booking_price) / Decimal(nights_nb)

    # Get tourist tax
    price_tourist_tax = Decimal(tourist_tax) * (
        Decimal(night_price) / Decimal(people_nb)
    )
    price_tourist_tax = price_tourist_tax / Decimal(100)
    if price_tourist_tax > max_tourist_tax:
        price_tourist_tax = max_tourist_tax
    # Add department percentage
    price_department_tax = (
        Decimal(department_tax) * Decimal(price_tourist_tax)
    ) / Decimal(100)
    total_tax = price_tourist_tax + price_department_tax

    # Calculate tax for whole stay
    getcontext().prec = 4
    result = (Decimal(people_nb) * Decimal(nights_nb)) * Decimal(total_tax)
    return ceil(result)


async def build_pdf_contract(rental_uid: str, db_booking: RentalBooking, test=False):
    html_contract = templates.TemplateResponse(
        request=None,
        name=f"./pdf_contract/contract_{db_booking.customer.language.lower()}.j2.html",
        context={
            "request": None,
            "booking": db_booking,
            "rental": db_booking.customer.rental,
            "deposit": db_booking.deposit,
            "tourist_tax": db_booking.tourist_tax,
            "locale": languages[db_booking.customer.language.lower()],
            "today": date.today(),
        },
    )
    if test:
        filename = f"./templates/pdf_contract/contract_{db_booking.customer.language.lower()}.pdf"  # noqa: E501
        HTML(string=html_contract.body).write_pdf(filename)
    else:
        dir_path = f"./static/rentals/{rental_uid}/bookings/{db_booking.start_at}"
        Path(dir_path).mkdir(parents=True, exist_ok=True)
        filename = f"{dir_path}/contract_{db_booking.customer.language.lower()}.pdf"
        HTML(string=html_contract.body).write_pdf(filename)
    return filename


async def build_contract_files(
    rental_uid: str,
    db_booking: RentalBooking,
    test=False,
):
    pdf_contract = await build_pdf_contract(rental_uid, db_booking, test)
    # Populate database
    db_booking.files = [
        RentalBookingFile(
            rental_id=rental_uid,
            filename=basename(value),
            booking_start_at=db_booking.start_at,
            booking_end_at=db_booking.end_at,
            position=index,
        )
        for index, value in enumerate([pdf_contract])
    ]

    return db_booking


async def send_booking_confirmation_email(db_booking: RentalBooking, test=False):
    if db_booking.should_send_email:
        if test:
            attachments = [db_booking.contract_file_test] + [
                db_file.get_path() for db_file in db_booking.customer.rental.files
            ]
        else:
            attachments = [db_file.get_path for db_file in db_booking.files] + [
                db_file.get_path() for db_file in db_booking.customer.rental.files
            ]
        message = MessageSchema(
            subject=(
                "Renting contract"
                if db_booking.customer.language == EnumLanguage.EN
                else "Contract de location"
            ),
            recipients=[db_booking.customer.email],
            bcc=[settings.MAIL_BBC],
            template_body={
                "booking": db_booking,
                "rental_address": db_booking.customer.rental.rental_address,
                "brand_name": db_booking.customer.rental.title,
                "landlord": {
                    "first_name": db_booking.customer.rental.landlord_first_name,
                    "last_name": db_booking.customer.rental.landlord_last_name,
                },
            },
            subtype="html",
            attachments=attachments,
        )
        await fm.send_message(
            message,
            template_name=f"contract_{db_booking.customer.language.lower()}.j2.html",
        )
        db_booking.confirmation_email_sent = True
    else:
        db_booking.confirmation_email_sent = False

    return db_booking
