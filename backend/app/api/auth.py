from __future__ import annotations

from fastapi import Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.main import app, get_async_db
from app.models.user import User
from app.schemas.user import Auth as AuthSchema
from app.settings.auth import auth_handler


async def authenticate_user(auth: AuthSchema, db: AsyncSession) -> dict:
    user = await db.get(User, auth.email)
    if user is None or not auth_handler.verify_password(auth.password, user.password):
        # Use a generic error message for security reasons
        raise HTTPException(status_code=401, detail="Invalid credentials")
    return {
        "access_token": auth_handler.encode_token(user.email),
        "email": user.email,
    }


@app.post("/login", tags=["auth"])
async def login(auth: AuthSchema, db: AsyncSession = Depends(get_async_db)) -> dict:
    user_tokens = await authenticate_user(auth, db)
    return {"tokens": user_tokens}
