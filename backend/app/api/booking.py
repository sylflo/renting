from __future__ import annotations

from datetime import date, timedelta
from math import ceil

from aiofiles import os
from fastapi import Depends, HTTPException
from sqlalchemy import func
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app.main import app, get_async_db
from app.models.booking import (
    RentalBooking,
    RentalBookingDate,
    RentalCustomer,
    get_booking_by_rental_and_dates,
)
from app.models.rental import get_rental_from_uid
from app.schemas.booking import Booking, BookingCreate, BookingList
from app.settings.auth import verify_access_token
from app.utils.booking import (
    build_contract_files,
    calculate_tourist_tax,
    send_booking_confirmation_email,
)


@app.get(
    "/rental/{rental_uid}/customers",
    status_code=200,
    tags=["customers"],
)
async def get_customers(
    rental_uid: str,
    skip: int = 0,
    limit: int = 100,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    customers = await db.execute(
        select(RentalCustomer)
        .offset(skip)
        .limit(limit)
        .filter(RentalCustomer.rental_id == rental_uid)
    )
    return customers.scalars().all()


@app.get(
    "/rental/{rental_uid}/bookings",
    response_model=BookingList,
    status_code=200,
    tags=["bookings"],
)
async def get_bookings(
    rental_uid: str,
    skip: int = 0,
    limit: int = 10,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    filtered_subquery = (
        select(RentalBooking).filter(RentalBooking.rental_id == rental_uid).subquery()
    )
    total = await db.execute(select(func.count()).select_from(filtered_subquery))
    query = await db.execute(
        select(RentalBooking)
        .offset(skip * limit)
        .limit(limit)
        .filter(RentalBooking.rental_id == rental_uid)
        .order_by(RentalBooking.start_at.desc())
    )
    bookings = query.scalars().all()
    return {
        "pages": (total.scalars().one() // 10) + 1,
        "bookings": [
            {
                "start_at": booking.start_at,
                "end_at": booking.end_at,
                "booking_price": booking.booking_price,
                "total_adult": booking.total_adult,
                "total_minor": booking.total_minor,
                "total_person": booking.total_person,
                "nights": booking.nights,
                "tourist_tax": booking.tourist_tax,
                "cleaning_price": booking.cleaning_price,
                "should_send_email": booking.should_send_email,
                "confirmation_email_sent": booking.confirmation_email_sent,
                "when_to_send_email": booking.when_to_send_email,
                "customer": {
                    "first_name": booking.customer.first_name,
                    "email": booking.customer.email,
                    "phone_number": booking.customer.phone_number,
                    "language": booking.customer.language,
                    "last_name": booking.customer.last_name,
                    "address": booking.customer.address,
                },
                "files": [
                    {
                        "filename": file.filename,
                        "position": file.position,
                        "get_path": file.get_path,
                    }
                    for file in booking.files
                ],
            }
            for booking in bookings
        ],
    }


async def create_booking_db(
    db_rental,
    rental_uid: str,
    booking: BookingCreate,
    db: AsyncSession,
    token=Depends(verify_access_token),
):
    customer_db = await db.get(RentalCustomer, (rental_uid, booking.email))
    nights = (booking.end_at - booking.start_at).days + 1
    if customer_db is None:
        customer_db = RentalCustomer(
            rental=db_rental,
            rental_id=rental_uid,
            email=booking.email,
            address=booking.address,
            phone_number=booking.phone_number,
            language=booking.language,
            first_name=booking.first_name,
            last_name=booking.last_name,
        )
    else:
        customer_db.phone_number = booking.phone_number
        customer_db.language = booking.language
        customer_db.first_name = booking.first_name
        customer_db.last_name = booking.last_name
        customer_db.address = booking.address

    if booking.tourist_tax:
        tourist_tax = calculate_tourist_tax(
            booking.booking_price,
            booking.total_adult,
            nights,
        )
    else:
        tourist_tax = 0
    cleaning_price = db_rental.cleaning_price if booking.cleaning else 0
    if booking.should_send_email:
        when_to_send_email = booking.when_to_send_email
    else:
        when_to_send_email = None
    booking_db = RentalBooking(
        start_at=booking.start_at,
        end_at=booking.end_at,
        nights=nights,
        tourist_tax=tourist_tax,
        cleaning_price=cleaning_price,
        booking_price=booking.booking_price,
        total_price=booking.booking_price + tourist_tax + cleaning_price,
        deposit=ceil((booking.booking_price * db_rental.deposit_percentage) / 100),
        total_adult=booking.total_adult,
        total_minor=booking.total_minor,
        customer=customer_db,
        dates=[
            RentalBookingDate(date=date)
            for date in (booking.start_at + timedelta(n) for n in range(nights))
        ],
        should_send_email=booking.should_send_email,
        when_to_send_email=when_to_send_email,
    )
    db.add(booking_db)
    return booking_db


@app.post(
    "/rentals/{rental_uid}/bookings",
    response_model=Booking,
    status_code=201,
    tags=["bookings"],
)
async def create_booking(
    rental_uid: str,
    booking: BookingCreate,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_rental = await get_rental_from_uid(rental_uid, db)
    if db_rental.landlord_email is None:
        raise HTTPException(
            status_code=403,
            detail="Please set your rental before adding a booking",
        )
    query = await db.execute(
        select(RentalBookingDate).filter(
            RentalBookingDate.date.between(booking.start_at, booking.end_at),
            RentalBookingDate.rental_id == rental_uid,
        )
    )
    dates = query.scalars().all()
    if len(dates) != 0:
        raise HTTPException(
            status_code=403,
            detail=f"Dates from {dates[0].date} to"
            f" {dates[-1].date} are already booked",
        )
    booking_db = await create_booking_db(db_rental, rental_uid, booking, db)
    booking_db = await build_contract_files(rental_uid, booking_db)
    await db.commit()
    return booking_db


@app.post(
    "/rentals/{rental_uid}/bookings/emails/confirmation/test",
    tags=["bookings"],
)
async def send_confirmation_email_test(
    rental_uid: str,
    booking: BookingCreate,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_rental = await get_rental_from_uid(rental_uid, db)
    booking.email = db_rental.landlord_email
    booking_db = await create_booking_db(db_rental, rental_uid, booking, db)
    if booking_db.customer.rental is None:
        booking_db.customer.rental = db_rental
    db_booking = await build_contract_files(rental_uid, booking_db, test=True)
    db_booking.should_send_email = True
    await send_booking_confirmation_email(db_booking, test=True)
    # Rollback cos we don't want the database to be populated
    await db.rollback()
    return True


@app.delete(
    "/rentals/{rental_uid}/bookings/{start}/{end}", status_code=204, tags=["bookings"]
)
async def delete_booking(
    rental_uid: str,
    start: date,
    end: date,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_booking = await get_booking_by_rental_and_dates(rental_uid, start, end, db)
    await db.delete(db_booking)
    for db_file in db_booking.files:
        await os.remove(
            f"static/rentals/{rental_uid}/bookings/{start}/{db_file.filename}"
        )
    await os.rmdir(f"static/rentals/{rental_uid}/bookings/{start}")
    await db.commit()
