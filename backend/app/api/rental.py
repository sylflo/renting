from __future__ import annotations

import asyncio
import uuid
import zoneinfo
from pathlib import Path
from typing import List

from aiofiles import open, os
from fastapi import Depends, File, HTTPException, UploadFile
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from werkzeug.utils import secure_filename

from app.config import settings
from app.main import app, get_async_db
from app.models.booking import find_booking_from_uid
from app.models.rental import Rental, RentalFile, get_rental_from_uid
from app.schemas.rental import Rental as RentalSchema
from app.schemas.rental import RentalCreate, RentalUpdate
from app.settings.auth import verify_access_token


@app.get("/rentals", response_model=List[RentalSchema], tags=["rentals"])
async def read_rentals(
    skip: int = 0,
    limit: int = 100,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    rentals = await db.execute(select(Rental).offset(skip).limit(limit))
    return rentals.scalars().all()


@app.get("/rentals/{rental_uid}", response_model=RentalSchema, tags=["rentals"])
async def get_rental(
    rental_uid: str,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_rental = await get_rental_from_uid(rental_uid, db)
    return db_rental


@app.post("/rentals", status_code=201, tags=["rentals"])
async def create_rental(
    rental: RentalCreate,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    uid = str(uuid.uuid4())
    db_rental = Rental(**rental.model_dump(), uid=uid)
    db.add(db_rental)
    await db.commit()
    await os.mkdir(f"static/rentals/{uid}")
    await os.mkdir(f"static/rentals/{uid}/bookings")
    await os.mkdir(f"static/rentals/{uid}/files")
    return db_rental


@app.put("/rentals/{rental_uid}", tags=["rentals"])
async def update_rental(
    rental_uid: str,
    rental: RentalUpdate,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_rental = await get_rental_from_uid(rental_uid, db)
    for key, value in rental.model_dump().items():
        setattr(db_rental, key, value)
    if db_rental.timezone not in zoneinfo.available_timezones():
        raise HTTPException(
            status_code=400,
            detail=f"Timezone {db_rental.timezone} does not exist, please choose an existing timezone",  # noqa: E501
        )
    db_rental.checkin_time = db_rental.checkin_time.replace(tzinfo=None)
    db_rental.checkout_time = db_rental.checkout_time.replace(tzinfo=None)
    await db.commit()
    return db_rental


@app.delete("/rentals/{rental_uid}", status_code=204, tags=["rentals"])
async def delete_rental(
    rental_uid: str,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    db_rental = await get_rental_from_uid(rental_uid, db)
    db_bookings = await find_booking_from_uid(rental_uid, db)
    if db_bookings:
        raise HTTPException(
            status_code=403, detail=f"Rental {db_rental.uid} has bookings"
        )

    await asyncio.gather(
        *(
            os.remove(f"static/rentals/{rental_uid}/files/{db_file.filename}")
            for db_file in db_rental.files
        )
    )
    try:
        await asyncio.gather(
            os.rmdir(f"static/rentals/{rental_uid}/files"),
            os.rmdir(f"static/rentals/{rental_uid}/bookings/"),
            os.rmdir(f"static/rentals/{rental_uid}"),
        )
    except OSError:
        raise HTTPException(
            status_code=403,
            detail=f"Rental {db_rental.uid} has files",
        )
    await db.delete(db_rental)
    await db.commit()


@app.delete("/rentals/{rental_uid}/files/{filename}", status_code=204, tags=["rentals"])
async def rental_delete_file(
    rental_uid: str,
    filename: str,
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    query = await db.execute(
        select(RentalFile).filter(
            RentalFile.rental_id == rental_uid, RentalFile.filename == filename
        )
    )
    db_file = query.scalars().first()
    if db_file is None:
        raise HTTPException(
            status_code=404,
            detail=f"File {filename} does not exist",
        )
    await os.remove(f"static/rentals/{rental_uid}/files/{filename}")
    await db.delete(db_file)
    await db.commit()


@app.post("/rentals/{rental_uid}/files", tags=["rentals"])
async def rental_upload_files(
    rental_uid: str,
    files: List[UploadFile] = File(...),
    db: AsyncSession = Depends(get_async_db),
    token=Depends(verify_access_token),
):
    if len(files) > settings.MAX_UPLOAD_FILES:
        raise HTTPException(
            status_code=403,
            detail=f"Maximum file upload excedeed: {settings.MAX_UPLOAD_FILES}",
        )
    dir_path = f"static/rentals/{rental_uid}/files"
    Path(dir_path).mkdir(parents=True, exist_ok=True)
    db_rental = await get_rental_from_uid(rental_uid, db)
    db_filenames = [file.filename for file in db_rental.files]
    for image in files:
        if image.content_type not in settings.ALLOWED_EXTENSIONS:
            raise HTTPException(
                status_code=403, detail=f"{image.filename} is not an image"
            )
        filename = secure_filename(str(image.filename))
        file_location = f"{dir_path}/{filename}"
        async with open(file_location, "wb") as file_object:
            content = await image.read()
            await file_object.write(content)
            if filename not in db_filenames:
                image = RentalFile(
                    filename=filename,
                    rental_id=rental_uid,
                    position=len(db_filenames),
                )
                db.add(image)
    await db.commit()
    return {"filenames": [file.filename for file in files]}
