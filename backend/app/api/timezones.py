import zoneinfo
from functools import lru_cache
from typing import List

from fastapi import Depends

from app.main import app


@lru_cache()
def timezones_list() -> List[str]:
    return list(zoneinfo.available_timezones())


@app.get("/timezones", response_model=List[str], tags=["timezones"])
def get_timezones(settings: List[str] = Depends(timezones_list)):
    return timezones_list()
