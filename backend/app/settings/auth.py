from __future__ import annotations

from datetime import datetime, timedelta, timezone
from typing import Any

import bcrypt
import jwt
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from pydantic import SecretStr

from app.config import settings

security = HTTPBearer()


class Auth:
    secret: SecretStr = settings.JWT_SECRET_KEY

    def encode_password(self, password: str) -> str:
        """Hashes a plain password using bcrypt."""
        salt = bcrypt.gensalt()
        return bcrypt.hashpw(password.encode(), salt).decode()

    def verify_password(self, password: str, hashed_password: str) -> bool:
        """Verifies a plain password against the hashed version using bcrypt."""
        return bcrypt.checkpw(password.encode(), hashed_password.encode())

    def encode_token(self, username: str) -> str:
        """Generates a JWT token for the given username."""
        payload: dict[str, Any] = {
            "exp": datetime.now(timezone.utc) + timedelta(days=settings.ACCESS_TOKEN_DURATION),  # noqa: E501
            "iat": datetime.now(timezone.utc),
            "scope": "access_token",
            "sub": username,
        }
        return jwt.encode(payload, self.secret.get_secret_value(), algorithm="HS256")

    def decode_token(self, token: str) -> Any:
        """Decodes the JWT token and returns the username if valid."""
        try:
            payload: dict[str, Any] = jwt.decode(
                token,
                self.secret.get_secret_value(),
                algorithms=["HS256"]
            )
            if payload.get("scope") == "access_token":
                return payload["sub"]
            raise HTTPException(status_code=401, detail="Scope for the token is invalid")  # noqa: E501
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401, detail="Token expired")
        except jwt.InvalidTokenError:
            raise HTTPException(status_code=401, detail="Invalid token")


auth_handler = Auth()


async def verify_access_token(
    credentials: HTTPAuthorizationCredentials = Security(security),
) -> Any:
    """Verifies the access token and returns the associated username."""
    token: str = credentials.credentials
    return auth_handler.decode_token(token)
