#!/bin/sh

set -e


GUNICORN_WORKERS=${GUNICORN_WORKERS:-1}

until pg_isready -h database -p 5432 -U postgres
do
  echo "Waiting for database to be up, retrying in 5 seconds"
  sleep 5
done
alembic upgrade head
gunicorn -b 0.0.0.0:8000 app.main:app -w $GUNICORN_WORKERS -k uvicorn.workers.UvicornWorker
